class CMZImage {
    loadBitmap(path, smooth) {
        var bitmap = ImageManager.loadBitmap("img/pictures/", path);
        bitmap.smooth = smooth !== false;
        return new Promise(function (resolve) {
            if (bitmap.isReady())
                resolve(bitmap);
            bitmap.addLoadListener(function () { resolve(bitmap); });
        });
    };

    showPicture(id, path, classes, jssscript) {
        if (!$gameScreen) return;
        var bitmapPath = path;
        var realPictureId = $gameScreen.realPictureId(id);
        var picture = new Game_Picture();
        picture.show(path, 0, 0, 0, 0, 0, 0, 0);
        $gameScreen._pictures[realPictureId] = picture;

        var jss = new JSS();
        if (typeof classes === "string") classes = classes.split(" ");
        if (Array.isArray(classes))
        for(var i = 0; i < classes.length; i++) {
            var cls = JSS.classes[classes[i]];
            if (cls) {
                cls = cls.__clone().__compile();
                jss = jss.__combine(cls, true);
            }
        }
        if (typeof jssscript === "string") {
            var cjss = JSS.parse(jssscript);
            jss = jss.__combine(cjss, true);
        }
        picture._style = jss;

        this.loadBitmap(bitmapPath).then(function (bitmap) {
            if (this._style && typeof this._style.apply == "function")
                this._style.apply(this, bitmap);
        }.bind(picture));
    };

    clearPicture(id) {
        if (!$gameScreen) return;
        if (Array.isArray(id)) {
            for (var i = 0; i < id.length; i++)
                this.clearPicture(id[i]);
            return;
        }
        if (id) {
            const realPictureId = $gameScreen.realPictureId(id);
            $gameScreen._pictures[realPictureId] = null;
        } else {
            for (var i = 1; i < $gameScreen._pictures.length; i++)
                $gameScreen._pictures[i] = null;
        }
    }
}

class JSS {
    __isJSS = true;
    __compile() {
        if (this.base && JSS.classes[this.base]) {
            var bs = JSS.classes[this.base].__clone().__compile();
            var re = this.__clone();
            re.__combine(bs, false);
            return re;
        }
        return this.__clone();
    }
    __combine(bs, override) {
        if (!bs) return this;
        override = override !== false;
        for(var k in JSS.validator) {
            if (bs[k] && (this[k] === undefined || this[k] === "" || override)) 
                this[k] = bs[k];
        }
        return this;
    }
    __clone() {
        var re = new JSS();
        re.__combine(this, true);
        return re;
    }

    apply(picture, bitmap) {
        var imageWidth = bitmap.width;
        var imageHeight = bitmap.height;

        var opt = this.calculate(imageWidth, imageHeight);
        if (cmz.debugMode)
            console.log("Applying picture style: ", picture, opt);
        picture.show(picture._name, opt.origin, opt.x, opt.y, opt.scaleX * 100, opt.scaleY * 100, opt.opacity * 255, opt.blend == "multiply" ? 1 : 0);
    }

    calculate(imageWidth, imageHeight, screenWidth, screenHeight) {
        screenHeight = Number(screenHeight || Graphics.height);
        screenWidth = Number(screenWidth || Graphics.width);

        var jss = this.__clone().__compile();
        var rh = JSS.getter(jss, "rh") || screenHeight;
        if (typeof rh === "string") {
            rh = Number(rh.substr(0, rh.length - 1)) / 100 * screenHeight;
        }
        var rw = JSS.getter(jss, "rw") || screenWidth;
        if (typeof rw === "string") {
            rw = Number(rw.substr(0, rw.length - 1)) / 100 * screenWidth;
        }

        var dw = imageWidth;
        var dh = imageHeight;

        var x = 0;
        var y = 0;

        var s = JSS.getter(jss, "s");
        if (s === "fit") {
            var scl = Math.min(rw / dw, rh / dh);
            dw *= scl;
            dh *= scl;
        }
        if (s === "fill") {
            var scl = Math.max(rw / dw, rh / dh);
            dw *= scl;
            dh *= scl;
        }
        if (s === "stretch") {
            dw = rw;
            dh = rh;
        }

        var mr = JSS.getter(jss, "mr") || 0;
        var ml = JSS.getter(jss, "ml") || 0;
        var mt = JSS.getter(jss, "mt") || 0;
        var mb = JSS.getter(jss, "mb") || 0;

        var a = JSS.getter(jss, "a").split(" ");
        if (a.includes("center")) x = (screenWidth - rw) / 2 + ml - mr;
        else if (a.includes("right")) x = screenWidth - rw - mr;
        else x = ml;
        if (a.includes("middle")) y = (screenHeight - rh) / 2 + mt - mb;
        else if (a.includes("bottom")) y = screenHeight - rh - mb;
        else y = mt;

        if (a.includes("center")) x += (rw - dw) / 2;
        else if (a.includes("right")) x += rw - dw;
        if (a.includes("middle")) y += (rh - dh) / 2;
        else if (a.includes("bottom")) y += rh - dh;

        var f = JSS.getter(jss, "f") || "none";
        if (f == "h" | f == "both") {
            x += dw;
            dw *= -1;
        }
        if (f == "v" | f == "both") {
            y += dh;
            dh *= -1;
        }

        var res = {
            origin: 0,
            x: x,
            y: y,
            scaleX: dw / imageWidth,
            scaleY: dh / imageHeight,
            opacity: JSS.getter(jss, "o") || 1,
            blend: JSS.getter(jss, "b") || "normal",

            destWidth: dw,
            destHeight: dh,
            sourceHeight: imageHeight,
            sourceWidth: imageWidth,
            rectWidth: rw,
            rectHeight: rh,
            alignment: a,
        }
        return res;
    }
}

JSS.classes = {};

JSS.getNumOrUndefined = function(v) {
    var num = Number(v);
    if (Number.isNaN(num)) return undefined;
    return v;
}

JSS.validator = {
    "align": {
        getter: (x) => (x.align || x.a || "").split(" ").where(x => ["left", "top", "right", "bottom", "center", "middle"].includes(x.toLowerCase())).select(x => x.toLowerCase()).join(" "),
    },
    "a": "align",
    
    "blending": {
        getter: (x) => ["multiply", "normal"].where(w => w == x.blending || w == x.b).first(),
    },
    "b": "blending",

    "flip": {
        getter: (x) => ["h", "v", "both", "none"].where(w => w == x.flip || w == x.f).first(),
    },
    "f": "flip",
    
    "scaling": {
        getter: (x) => ["fill", "stretch", "fit", "none"].where(w => w == x.stretch || w == x.s).first(),
    },
    "s": "scaling",

    "rectheight": {
        getter: (x) => {
            var bval = x.rectheight || x.rh || "";
            var val = Number(bval);
            if (Number.isNaN(val) && bval.substr(bval.length - 1) == "%") {
                val = Number(bval.substr(0, bval.length - 1));
                if (!Number.isNaN(val)) return val + "%";
                else return undefined;
            }
            else if (Number.isNaN(val)) return undefined;
            return val;
        }
    },
    "rh": "rectheight",

    "rectwidth": {
        getter: (x) => {
            var bval = x.rectwidth || x.rw || "";
            var val = Number(bval);
            if (Number.isNaN(val) && bval.substr(bval.length - 1) == "%") {
                val = Number(bval.substr(0, bval.length - 1));
                if (!Number.isNaN(val)) return val + "%";
                else return undefined;
            }
            else if (Number.isNaN(val)) return undefined;
            return val;
        }
    },
    "rw": "rectwidth",

    "marginright": {
        getter: (x) => JSS.getNumOrUndefined(x.marginright || x.mr)
    },
    "mr": "marginright",

    "marginleft": {
        getter: (x) => JSS.getNumOrUndefined(x.marginleft || x.ml)
    },
    "ml": "marginleft",

    "margintop": {
        getter: (x) => JSS.getNumOrUndefined(x.margintop || x.mt)
    },
    "mt": "margintop",

    "marginbottom": {
        getter: (x) => JSS.getNumOrUndefined(x.marginbottom || x.mb)
    },
    "mb": "marginbottom",

    "opacity": {
        getter: (x) => JSS.getNumOrUndefined(x.opacity || x.o)
    },
    "o": "opacity",

    "base": {
        getter: (x) => x.base || undefined,
    }
};

JSS.getter = function(jss, prop) {
    if (typeof jss !== "object" || typeof prop !== "string") return undefined;
    var k = prop.toLowerCase();
    var v = JSS.validator[k];
    if (typeof v === "string") v = JSS.validator[v];
    if (v) return v.getter(jss);
}

JSS.parse = function(str) {
    if (typeof str !== "string" && typeof str !== "object") return;
    var json = null;
    if (typeof str === "string") {
        try {
            json = JSON.parse("{" + str + "}");
        } catch {
            try {
                json = JSON.parse(str);
            } 
            catch{

            }
        }
    } else json = str;

    if (typeof json !== "object" || !json) return;

    if (Array.isArray(json)) {
        for(var i=0; i < json.length; i++)
            JSS.parse(json[i]);
        return;
    }

    var jss = new JSS();
    for(var k in JSS.validator) {
        if (json[k] !== undefined)
            jss[k] = JSS.getter(json, k);
    }
    jss = jss.__compile();
    if (typeof json.__class === "string")
    {
        var c = JSS.classes[json.__class];
        if (c) {
            jss = c.__combine(jss);
        }
        JSS.classes[json.__class] = jss;
    }
    return jss.__clone();
}