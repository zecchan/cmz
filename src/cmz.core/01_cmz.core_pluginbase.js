// Override this class then call cmz.registerPlugin(new YourPluginClass());
class CMZPluginBase {
    // metadata of this plugin
    _meta = {
        name : "Plugin",
        id : "pluginbase",
        author : "Code Atelier",
        version : new Version(1, 0, 0),
        cmzMinVersion : new Version(1, 0, 0),

        require : [],
        /*
        require : [
            {
                id: "someplugin",
                version: [1, 0]
            }
        ]; 
        */
    }

    // this will be saved along with savedata, this data will also be replaced when the game is loading a savedata and emptied when the player starts a new game
    data = {}

    // override for initialization
    initialize() {}

    // override for data initialization for new game
    resetData() {}

    // override to receive message pump from cmz
    receiveMessage(msgid, data) {};
    
    // override to automatically update args when the game load save data, it should be designed to update cmz.eval.Args only.
    updateArgs() {}

    // Below code is used for CMZ internal process, do not change or override it.
    __cmzpluginbase = true;
    __cmzcheckver() {
        if (!cmz.checkVersion(this._meta.cmzMinVersion)) {
            console.error("Plugin '" + this._meta.name + "' requires higher version of CMZ (" + this._meta.cmzMinVersion.toString() + ")");
            return false;
        }
        return true;
    }
    __cmzcheckrequire() {
        var allOk = true;
        for(var i = 0; i < this._meta.require.length; i++) {
            var r = this._meta.require[i];
            if (!cmz.pluginLoaded(r.id, r.version)) {
                allOk = false;
                break;
            }
        }
        return allOk;
    }

}