//=============================================================================
// Coatl RPG Maker MZ - Core Plugin
//=============================================================================

/*:
 * @target MZ
 * @plugindesc Contains core functionality for CMZ Plugins.
 * @author Zecchan Silverlake
 *
 * @help cmz.core.js v1.1.0
 *
 * This plugin contains necessary logic for CMZ Plugins.
 * 
 * @param debugMode
 * @text Debug Mode
 * @type boolean
 * @on Enabled
 * @off Disabled
 * @desc This will show logs in console when enabled
 * @default false
 * 
 * @param allow4WayPassageForAbovePlayer
 * @text Allow 4 Way for Above
 * @type boolean
 * @on Enabled
 * @off Disabled
 * @desc When enabled, 4 way passage also applies to above-player tiles
 * @default true
 *
 * @param jss
 * @type multiline_string
 * @text JSS
 * @desc JSON Style Sheet that will be loaded before any other JSS.
 * @default {
"__class": "cutin",
"s": "fit",
"a": "bottom right",
"rh": "80%",
"rw": "50%"
},{
"__class": "event",
"s": "fill",
"a": "bottom center"
}
 * 
 * @param colordb
 * @text Color Database
 * @type struct<ColorStruct>[]
 * @default ["{\"id\":\"white\",\"color\":\"0\"}","{\"id\":\"lightblue\",\"color\":\"1\"}","{\"id\":\"lightred\",\"color\":\"2\"}","{\"id\":\"lightgreen\",\"color\":\"3\"}","{\"id\":\"lightteal\",\"color\":\"4\"}","{\"id\":\"lightpurple\",\"color\":\"5\"}","{\"id\":\"lightyellow\",\"color\":\"6\"}","{\"id\":\"gray\",\"color\":\"7\"}","{\"id\":\"lightgray\",\"color\":\"8\"}","{\"id\":\"blue\",\"color\":\"9\"}","{\"id\":\"red\",\"color\":\"10\"}","{\"id\":\"green\",\"color\":\"11\"}","{\"id\":\"teal\",\"color\":\"12\"}","{\"id\":\"purple\",\"color\":\"13\"}","{\"id\":\"yellow\",\"color\":\"14\"}","{\"id\":\"black\",\"color\":\"15\"}","{\"id\":\"purpleblue\",\"color\":\"16\"}","{\"id\":\"pureyellow\",\"color\":\"17\"}","{\"id\":\"crimson\",\"color\":\"18\"}","{\"id\":\"lightblack\",\"color\":\"19\"}","{\"id\":\"orange\",\"color\":\"20\"}","{\"id\":\"lightorange\",\"color\":\"21\"}","{\"id\":\"skyblue\",\"color\":\"22\"}","{\"id\":\"lightskyblue\",\"color\":\"23\"}","{\"id\":\"lime\",\"color\":\"24\"}","{\"id\":\"brown\",\"color\":\"25\"}","{\"id\":\"lightbrown\",\"color\":\"25\"}","{\"id\":\"deeppurple\",\"color\":\"26\"}","{\"id\":\"fuchsia\",\"color\":\"27\"}","{\"id\":\"grassgreen\",\"color\":\"28\"}","{\"id\":\"lightgrassgreen\",\"color\":\"29\"}","{\"id\":\"magenta\",\"color\":\"30\"}","{\"id\":\"lightmagenta\",\"color\":\"31\"}","{\"id\":\"npc\",\"color\":\"6\"}","{\"id\":\"narrator\",\"color\":\"4\"}","{\"id\":\"self\",\"color\":\"0\"}","{\"id\":\"item\",\"color\":\"5\"}","{\"id\":\"gold\",\"color\":\"14\"}"]
 *
 * @param globalConditional
 * @type struct<GlobalConditionalStruct>[]
 * @text Global Condition Eval
 * @desc Scripts that will be run before event conditional eval. Resulting vars will be injected to eval. Global eval does not proc another global eval.
 * @default []
 * 
 * @command conditions
 * @text Add Conditions
 * @desc Add more condition for this Event using JavaScript.
 * (Multiple instances will be joined by AND operator)
 *
 * @arg script
 * @type multiline_string
 * @text Script
 * @desc JavaScript that evaluates to boolean.
 * 
 */

/*~struct~ColorStruct:
 *
 * @param id
 * @text ID
 * @type string
 * @desc The ID of the color. Only alphanumeric and no space, it must start with alphabet.
 * @default
 * 
 * @param color
 * @text Color value
 * @type number
 * @desc The color value based on the system color id.
 * @default 0
 */

/*~struct~GlobalConditionalStruct:
 *
 * @param variable
 * @text Variable Name
 * @type string
 * @desc The name of JS variable that will be passed to eval.
 * @default
 * 
 * @param script
 * @type multiline_string
 * @text Script
 * @desc JavaScript that evaluates to value.
 * 
 */

class Version {
    Major = 0;
    Minor = null;
    Build = null;
    
    constructor(major, minor, build) {
        this.__class = "Version";
        if (typeof major == "string") {
            var spl = major.split(".");
            if (spl.length > 1) {
                major = spl[0];
                minor = spl[1];
                if (spl.length > 2)
                    build = spl[2];
            }            
        }
        if (typeof major === "object" && major.__class === "Version") {
            minor = major.Minor;
            build = major.Build;
            major = major.Major;
        }
        
        this.Major = Number.isNaN(Number(major)) ? 0 : Number(major);
        this.Minor = Number.isNaN(Number(minor)) ? null : Number(minor);
        this.Build = Number.isNaN(Number(build)) ? null : Number(build);
    }

    compare(a, b) {
        if (a === undefined || a === null) return false;
        if (b === undefined || b === null) {
            b = a;
            a = this.toString();
        }
        if (typeof a === "string") {
            a = new Version(a);
        }
        if (typeof b === "string") {
            b = new Version(b);
        }
        if (a.__class === "Version" && b.__class === "Version") {
            if (a.Major > b.Major) return 1;
            if (a.Major < b.Major) return -1;
            if (a.Minor !== null && b.Minor === null || a.Minor > b.Minor) return 1;
            if (a.Minor === null && b.Minor !== null || a.Minor < b.Minor) return -1;
            if (a.Build !== null && b.Build === null || a.Build > b.Build) return 1;
            if (a.Build === null && b.Build !== null || a.Build < b.Build) return -1;
        } 
        return 0;
    }

    equal(b) {
        return this.compare(b) == 0;
    }

    greaterThan(b) {
        return this.compare(b) > 0;
    }

    greaterOrEqualTo(b) {
        return this.compare(b) >= 0;
    }

    lessThan(b) {
        return this.compare(b) < 0;
    }

    lessOrEqualTo(b) {
        return this.compare(b) <= 0;
    }

    toString() {
        var ret = this.Major;
        if (this.Minor !== null)
            ret = ret + "." + this.Minor;
        if (this.Build !== null)
            ret = ret + "." + this.Build;
        return ret;
    }
}

const __cmzversion = new Version(1, 1, 0);