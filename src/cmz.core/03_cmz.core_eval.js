class CMZEvaluator {
    constructor() {
        this.Args = {};
    }

    exec(script, fromGlobal) {
        var keys = [];
        var values = [];

        // statics
        this.Args.system = {
            mapId: $gameMap ? $gameMap._mapId : null,
            partyCount: $gameParty ? $gameParty._actors.length : 1,
        };

        for (var k in this.Args) {
            keys.push(k);
            values.push(this.Args[k]);
        }

        if (!fromGlobal) {
            var gc = mz.pluginParameter("cmz.core").globalConditional;
            if (Array.isArray(gc)) {
                for(var i = 0; i < gc.length; i++) {
                    var c = gc[i];
                    if (c.variable && typeof c.variable === "string" && !keys.includes(c.variable) && c.script) {
                        var ret = this.exec(c.script, true);
                        keys.push(c.variable);
                        values.push(ret);
                    }
                }
            }
        }

        var js = "return " + script;
        var f = new Function(keys, js);
        try {
            var val = f.apply(this.Args, values);
            if (fromGlobal) return val;
            if (!val) return false;
        }
        catch(ex) {
            console.error(ex);
            return false;
        }
        return true;
    }

    execValue(script) {
        var keys = [];
        var values = [];

        // statics
        this.Args.system = {
            mapId: $gameMap ? $gameMap._mapId : null,
        };

        for (var k in this.Args) {
            keys.push(k);
            values.push(this.Args[k]);
        }

        var gc = mz.pluginParameter("cmz.core").globalConditional;
        if (Array.isArray(gc)) {
            for(var i = 0; i < gc.length; i++) {
                var c = gc[i];
                if (c.variable && typeof c.variable === "string" && !keys.includes(c.variable) && c.script) {
                    var ret = this.exec(c.script, true);
                    keys.push(c.variable);
                    values.push(ret);
                }
            }
        }

        var js = "return " + script;
        var f = new Function(keys, js);
        try {
            var val = f.apply(this.Args, values);
            return val;
        }
        catch(ex) {
            console.error(ex);
            return false;
        }
    }
}