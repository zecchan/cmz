class CMZFunctionHook {
    constructor(base, functionName, options) {
        this.hookid = "hook_" + Math.round(Math.random() * 100000);
        
        if (base) {
            var hook = this;
            var fHook = function() {
                var args = [];
                for(var i = 0; i < arguments.length; i++)
                    args.push(arguments[i]);
                return hook.exec(this, args);
            };

            if (base['prototype'][functionName]) {
                this.original = base['prototype'][functionName];
                base['prototype'][functionName] = fHook;
            }
            else if (base[functionName]) {
                this.original = base[functionName];
                base[functionName] = fHook;
            }
        }
     
        options = options || {};

        this.runOriginal = (options.runOriginal === false ? false : true) && base ? true : false;
        this.defaultPosition = options.defaultPosition || 'after';
    }

    lastReturn = undefined;
    shouldHalt = false;
    halt() {
        this.shouldHalt = true;
    }

    caller = null;
    __callerStack = [];

    __before = [];
    __after = [];

    config(f) {
        f.apply(this);
        return this;
    }

    before(f) {
        this.__before.splice(0, 0, f);
        return this;
    }

    after(f) {
        this.__after.push(f);
        return this;
    }

    add(f) {
        if (this.defaultPosition == 'before') this.before(f);
        else this.after(f);
        return this;
    }

    __preCall = [];
    preCall(f) {
        this.__preCall.push(f);
        return this;
    }
    execPreCall(args) {
        for(var i = 0; i < this.__preCall.length; i++) {
            this.__preCall[i].apply(this, [args]);
        }
    }

    __postCall = [];
    postCall(f) {
        this.__postCall.push(f);
        return this;
    }
    execPostCall(args) {
        for(var i = 0; i < this.__postCall.length; i++) {
            this.__postCall[i].apply(this, [args]);
        }
    }

    exec(that, args) {
        that = that || this;
        if (this.caller)
            this.__callerStack.push(this.caller);
        this.caller = that;
        this.lastReturn = undefined;
        this.shouldHalt = false;

        // before
        for(var i = 0; i < this.__before.length; i++) {
            this.execPreCall(args);
            this.lastReturn = this.__before[i].apply(this, args);
            this.execPostCall(args);
            if (this.shouldHalt)
                return this.lastReturn;
        }

        // original
        if (this.runOriginal) {
            this.execPreCall(args);
            this.lastReturn = this.original.apply(that, args);
            this.execPostCall(args);
            if (this.shouldHalt)
                return this.lastReturn;
        }

        // after
        for(var i = 0; i < this.__after.length; i++) {
            this.execPreCall(args);
            this.lastReturn = this.__after[i].apply(this, args);
            this.execPostCall(args);
            if (this.shouldHalt)
                return this.lastReturn;
        }

        if (this.__callerStack.length > 0)
            this.caller = this.__callerStack.pop();
        else
            this.caller = null;
        return this.lastReturn;
    }
}