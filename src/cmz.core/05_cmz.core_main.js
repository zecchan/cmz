class CMZ {
    constructor() {
        this.version = __cmzversion;
        var param = mz.pluginParameter("cmz.core");
        this.debugMode = param.debugMode === true;
        this.eval = new CMZEvaluator();
        this.images = new CMZImage();
        var jss = param.jss;
        if (typeof jss === "object")
            jss = JSON.stringify(jss);
        JSS.parse("[" + jss + "]");

        if (param.allow4WayPassageForAbovePlayer) {
            Game_Map.prototype.checkPassage = function(x, y, bit) {
                const flags = this.tilesetFlags();
                const tiles = this.allTiles(x, y);
                for (const tile of tiles) {
                    const flag = flags[tile];
                    if ((flag & 0x10) !== 0) {
                        // [*] No effect on passage
                        if ((flag & bit) === bit)
                            return false;
                        continue;
                    }
                    if ((flag & bit) === 0) {
                        // [o] Passable
                        return true;
                    }
                    if ((flag & bit) === bit) {
                        // [x] Impassable
                        return false;
                    }
                }
                return false;
            };
        }
    }

    __runOnce = [];
    runOnce(id, func) {
        if (typeof id !== "string") return;
        if (this.__runOnce.includes(id)) return;
        if (typeof func === "function") {
            this.__runOnce.push(id);
            func();
        }
    }

    checkVersion(ver) {
        return this.version.greaterOrEqualTo(new Version(ver));
    }

    __lastTick = null;
    __tick() {
        var time = Number(new Date());
        if (!this.__lastTick) {
            this.__lastTick = time;
        } 
        var delta = time - this.__lastTick;
        this.__lastTick = time;
        this.events.onTick.exec(this, [delta]);
    };
    __ticker;

    plugins = {};
    __tempPlugins = [];
    registerPlugin(p) {
        if (p && p.__cmzpluginbase) {
            this.__tempPlugins.push(p);
        }
    }

    pluginLoaded(id, ver) {
        if (!this.plugins[id]) return false;
        var v = this.plugins[id]._meta.version;
        return v.greaterOrEqualTo(ver);
    }

    pumpMessage(msgid, data) {
        for(var k in this.plugins) {
            this.plugins[k].receiveMessage(msgid, data);
        }
    }

    initialize() {
        if (this.debugMode) console.log("Coatl RPG MZ Core System v" + this.version.toString());
        while(this.__tempPlugins.length > 0) {
            var found = false;
            for(var i = 0; i < this.__tempPlugins.length; i++) {
                var pl = this.__tempPlugins[i];
                if (pl.__cmzcheckrequire()) {
                    this.plugins[pl._meta.id] = pl;
                    pl.initialize();
                    this.__tempPlugins.splice(i, 1);
                    found = true;
                    if (this.debugMode) console.log("Plugin '" + pl._meta.name + " v" + pl._meta.version.toString() + "' by " + pl._meta.author + " loaded." )
                    break;
                }
            }
            if (found) continue;
            // should not reach here
            for(var i = 0; i < this.__tempPlugins.length; i++) {
                var pl = this.__tempPlugins[i];
                for(var i = 0; i < pl._meta.require.length; i++) {
                    var r = pl._meta.require[i];
                    if (!cmz.pluginLoaded(r.id, r.version)) {
                        var err = "Plugin '" + pl._meta.name + " v" + pl._meta.version.toString() + "' requires '" + r.id + "' v" + r.version.toString() + "' to work.";
                        if (this.debugMode)
                            console.error(err);
                        throw new Error(err);
                    }
                }
            }
            if (this.debugMode)
                console.error("Plugin '" + pl._meta.name + " v" + pl._meta.version.toString() + "' cannot be loaded.");
        }

        if (!this.__ticker) {
            var thisArg = this;
            this.__ticker = setInterval(function() {
                thisArg.__tick();
            }, 25);
        }
    }

    events = {
        onGameSave : new CMZFunctionHook(DataManager, "makeSaveContents").preCall(function(arg) {
            if (Array.isArray(arg)) {
                if (this.lastReturn) {
                    while(arg.length > 0) arg.pop();
                    arg.push(this.lastReturn)
                } else {
                    this.lastReturn = arg[0];
                }
            }
        }).after(function(data) {
            data.pluginData = {};
            for(var k in cmz.plugins) {
                    data.pluginData[k] = cmz.plugins[k].data;
            } 
            if (cmz.debugMode)
                console.log("Save Data", data);
            return data;
        }),

        onGameLoad : new CMZFunctionHook(DataManager, "extractSaveContents").after(function(data){
            if (cmz.debugMode)
                console.log("Load Save Data", data);
            if (data.pluginData) {
                for(var k in data.pluginData) {
                    if (cmz.plugins[k]) {
                        if (cmz.debugMode)
                            console.log("Loading data for '" + k + "'");
                        cmz.plugins[k].data = data.pluginData[k];
                        if (typeof cmz.plugins[k].updateArgs === "function")
                            cmz.plugins[k].updateArgs();
                    }
                }
            }
            
            if (cmz.debugMode)
                console.log("Save data loaded");
        }),

        onTick: new CMZFunctionHook(),
        onFrameTick: new CMZFunctionHook(Graphics, "_onTick"),

        onAddMenuCustomCommand: new CMZFunctionHook(Window_MenuCommand, "addOriginalCommands"),
        onCreateCommandWindow: new CMZFunctionHook(Scene_Menu, "createCommandWindow"),
        onMenuShown: new CMZFunctionHook(Scene_Menu, "start"),

        onCheckEventPage: new CMZFunctionHook(Game_Event, "meetsConditions").after(function(page) {
            if (this.lastReturn === false) return false;
    
            for (var i = 0; i < page.list.length; i++) {
                var c = page.list[i];
                if (c.code == 357 && c.parameters[0] == "cmz.core" && c.parameters[1] == "conditions") {
                    var val = cmz.eval.exec(c.parameters[3].script);
                    if (!val) return false;
                }
            }

            var nop = {code: -1, parameters: [], indent: 0};
            var nl = [];
            var pc = null;
            for (var i = 0; i < page.list.length; i++) {
                var c = page.list[i];
                if (pc == null) {
                    pc = c;
                    nl.push(c);
                } else {
                    // chaining choices function -- remove any 404 that immediately followed by 102 in the same indent
                    if (pc.code == 404 && c.code == 102 && pc.indent == c.indent) {
                        nl.pop();
                        pc = null;

                        // find the index prev 102 and next 404 on the same indent
                        var indent = c.indent;
                        var p102 = -1;
                        var n404 = -1;
                        for(var j = i - 2; j >= 0; j--) {
                            var jc = page.list[j];
                            if (jc.indent == indent && jc.code == 102) {
                                p102 = j;
                                break;
                            }
                        }
                        for(var j = i + 1; j < page.list.length; j++) {
                            var jc = page.list[j];
                            if (jc.indent == indent && jc.code == 404) {
                                n404 = j;
                                break;
                            }
                        }

                        if (p102 >= 0 && n404 >= 0) {
                            var cHead = page.list[p102];
                            var np0 = [];
                            for(var j = p102; j < n404; j++) {
                                var jc = page.list[j];
                                if (jc.code == 402 && jc.indent == indent) {
                                    np0.push(jc.parameters[1]);
                                }
                            }
                            var pl = cHead.parameters[0].length;
                            cHead.parameters[0] = np0;
                            if (cHead.parameters[1] < 0) cHead.parameters[1] = pl + c.parameters[1];
                            if (cHead.parameters[2] < 0) cHead.parameters[2] = pl + c.parameters[2];
                        }

                        page.list[i] = nop;
                        page.list[i-1] = nop;
                        continue;
                    }

                    pc = c;
                    nl.push(c);
                }
            }
            page.list = nl;
    
            return true;
        }),

        onGenerateEventList: new CMZFunctionHook(Game_Event, "list").after(function() {
            var list = this.lastReturn;
            return list;
        }),

        onGenerateMessageText: new CMZFunctionHook(Game_Message, "allText"),
        onClearMessageText: new CMZFunctionHook(Game_Message, "clear"),

        onMapStart: new CMZFunctionHook(Scene_Map, "start"),

        onConvertEscapeCharacter: new CMZFunctionHook(Window_Base, "convertEscapeCharacters").preCall(function(args) {
            if (this.lastReturn)
                args[0] = this.lastReturn;
        }).before(function(text) {
            text = text.replace(/\\/g, '\x1b');
            text = text.replace(/\x1bii\[([0-9]+)\]/gi, function (match, id) {
                var item = $dataItems[id];
                if (item)
                    return "\\i[" + item.iconIndex + "] \x1bc[item]" + item.name + "\x1bc[0]";
                return match;
            });
            text = text.replace(/\x1bia\[([0-9]+)\]/gi, function (match, id) {
                var item = $dataArmors[id];
                if (item)
                    return "\\i[" + item.iconIndex + "] \x1bc[item]" + item.name + "\x1bc[0]";
                return match;
            });
            text = text.replace(/\x1biw\[([0-9]+)\]/gi, function (match, id) {
                var item = $dataWeapons[id];
                if (item)
                    return "\\i[" + item.iconIndex + "] \x1bc[item]" + item.name + "\x1bc[0]";
                return match;
            });
            var colors = mz.pluginParameter("cmz.core").colordb;
            text = text.replace(/\#(\w+)\[(\w+)\]/gi, function(match, col, content) {
                console.log(col, content);
                return "\x1bC["+col+"]"+content+"\x1bC[0]";
            });
            text = text.replace(/\x1bC\[(\w+)\]/gi, function (match, color) {
                for(var i = 0; i < colors.length; i++) {
                    if (colors[i].id == color)
                        return "\\c[" + Number(colors[i].color) + "]";
                }
                return "\\c[" + color + "]";
            });
            return text;
        }),

        onShopProcessing: new CMZFunctionHook(Game_Interpreter, "command302", {runOriginal: false})
        .config(function() {
            this.canSell = function(type, id) {
                var ds = type == 0 ? $dataItems : type == 1 ? $dataWeapons : type == 2 ? $dataArmors : [];
                var item = ds[id];
                if (!item)
                    return false;
                var meta = item.meta;
                if (!meta.sellif)
                    return true;
                var exec = meta.sellif;
                var val = cmz.eval.exec(exec);
                return val ? true : false;
            };
        })
        .before(function(params) {
            if (!$gameParty.inBattle()) {
                const goods = [];
                if (this.canSell(params[0], params[1]))
                    goods.push(params);
                while (this.caller.nextEventCode() === 605) {
                    this.caller._index++;
                    var nParam = this.caller.currentCommand().parameters;
                    if (this.canSell(nParam[0], nParam[1]))
                        goods.push(nParam);
                }
                this.goods = goods;
                console.log(goods);
            }
        })
        .after(function(params) {
            if (!$gameParty.inBattle()) {
                SceneManager.push(Scene_Shop);
                SceneManager.prepareNextScene(this.goods, params[4]);
            }
            return true;
        })
    };
}

class MZHelper {
    pluginParameter(jsname) {
        for(var i = 0; i < $plugins.length; i++) {
            if ($plugins[i].name == jsname) {
                var param = $plugins[i].parameters;
                return this.sanitizePluginParameter(param);
            }
        }
        return {};
    }

    sanitizePluginParameter(param) {
        if (typeof param === "object") {
            for(var k in param) {
                param[k] = this.sanitizePluginParameter(param[k]);
            }
            return param;
        } else if (Array.isArray(param)) {
            for(var i = 0; i < param.length; i++) {
                param[i] = this.sanitizePluginParameter(param[i]);
            }
            return param;
        } else if (typeof param === "string") {
            try {
                var js = JSON.parse(param);
                return this.sanitizePluginParameter(js);
            }
            catch{}
            return param;
        } else return param;
    }

    makeTint(red, green, blue, saturation) {
        
    }

    isEventRunning() {
        return $gameMap ? $gameMap.isEventRunning() : false;
    }

    isInMapScene() {
        return SceneManager._scene && SceneManager._scene.__class == "Scene_Map";
    }

    isGameActive() {
        return SceneManager.isGameActive();
    }

    requestEventRefresh() {
        if ($gameMap)
            $gameMap.requestRefresh();
    }

    var(id, value) {
        if (value === undefined) 
            return $gameVariables.value(id);
        $gameVariables.setValue(id, value);
    }

    v(id, value) {
        return this.var(id, value);
    }

    switch(id, value) {
        if (value === undefined) 
            return $gameSwitches.value(id);
        $gameSwitches.setValue(id, value ? true : false);
    }

    s(id, value) {
        return this.switch(id, value);
    }
}

// hook to window load
window.addEventListener("load", () => cmz.initialize());

// flags
Scene_Map.prototype.__class = "Scene_Map";

// Pathfinding
class AStarNode {
    constructor(astar, parent, x, y) {
        this.astar = astar;
        this.parent = parent;
        this.h = Math.abs(astar.targetX - x) + Math.abs(astar.targetY - y);
        this.x = x;
        this.y = y;
        this.apos = this.toCoord(true);
    }

    toCoord(full) {
        var res = "(" + this.x + ", " + this.y + ")";
        if (full === true && this.parent && this.parent.toCoord)
            res += " << " + this.parent.toCoord(true);
        return res;
    }

    getNeighbors() {
        var res = [];
        var d = [[-1, 0], [1, 0], [0, -1], [0, 1]];
        for(var i = 0; i < d.length; i++) {
            var dx = d[i][0];
            var dy = d[i][1];
            var tx = this.x + dx;
            var ty = this.y + dy;
            var closed = this.astar.getFromClosed(tx, ty);
            if (this.astar.checkViable(this.x, this.y, tx, ty)) {
                if (!closed) {
                    var nnode = new AStarNode(this.astar, this, tx, ty);
                    nnode.g = this.g + 1;
                    res.push(nnode);
                }
            }
        }
        return res;
    }

    apos = null;
    astar = null;
    parent = null;

    getf() {
        return this.g + this.h;
    }

    toString() {
        return "(" + this.x + ", " + this.y + ") {g: " + this.g + ", h:" + this.h + "} target: (" + this.astar.targetX + ", " + this.astar.targetY + ")";
    }
    
    g = 0;
    h = 0;
    x = 0;
    y = 0;
}

class AStar {
    minX = 0;
    minY = 0;
    maxX = 100;
    maxY = 100;

    open = [];
    closed = [];

    targetX = 0;
    targetY = 0;

    getFromOpen(x, y) {
        var list = this.open;
        for(var i = 0; i < list.length; i++) {
            var node = list[i];
            if (!node) console.log(i, this.open);
            if (node.x == x && node.y == y) return node;
        }
        return null;
    }

    getFromClosed(x, y) {
        var list = this.closed;
        for(var i = 0; i < list.length; i++) {
            var node = list[i];
            if (!node) console.log(i, this.closed);
            if (node.x == x && node.y == y) return node;
        }
        return null;
    }

    run() {
        // find the lowest f in open
        var minF = 99999999;
        var lowest = -1;
        for(var i = 0; i < this.open.length; i++) {
            var w = this.open[i].getf();
            if (w < minF) {
                minF = w;
                lowest = i;
            }
        }
        if (lowest < 0) {
            // no solution
            return false;
        }

        // remove from open and add to closed
        var node = this.open[lowest];
        this.open.splice(lowest, 1);
        this.closed.push(node);
        if (node.x == this.targetX && node.y == this.targetY) return node;

        var ng = node.getNeighbors();
        // put those nodes to open
        for(var i = 0; i < ng.length; i++) {
            var nd = ng[i];
            var inOpen = this.getFromOpen(nd.x, nd.y);
            if (!inOpen) {
                nd.parent = node;
                nd.g = node.g + 1;
                this.open.push(nd);
            } else {
                if (inOpen.g > nd.g) {
                    inOpen.parent = node;
                    inOpen.g = nd.g;
                }
            }
        }
        var res = this.run();
        return res;
    }

    findRoute(sX, sY, tX, tY) {
        var starting = new AStarNode(this, null, sX, sY);
        this.targetX = tX;
        this.targetY = tY;
        this.open = [starting];
        this.closed = [];

        return this.run();
    }

    checkValid(x, y, x2, y2) {
        return x2 >= this.minX && y2 >= this.minY && x2 <= this.maxX && y2 <= this.maxY && this.checkViable(x, y, x2, y2);
    }

    checkViable(x, y, x2, y2) {
        return false;
    }
}

// Extensions
Game_Character.prototype.findNextRouteTo = function(x, y) {
    var astar = new AStar();
    if ($gameMap) {
        astar.maxX = $gameMap.width() - 1;
        astar.maxY = $gameMap.height() - 1;
    }
    astar.checkViable = function(x, y, x2, y2) {
        var dir = x < x2 ? 6 : x > x2 ? 4 : y > y2 ? 2 : y < y2 ? 8 : 0;
        var res = this.canPass(x2, y2) //, dir); <== disabled because sometime gives invalid reading
        //console.log([x , y] + " =" + dir + "=> " + [x2, y2] + " : "  + res);

        return res;
    }.bind(this);
    var route = astar.findRoute(this._x, this._y, x, y);
    if (route) {
        var prt = route.parent;
        var rt = route;
        while(prt !== null) {
            if (prt.x == this._x && prt.y == this._y) {
                return [rt.x, rt.y];
            }
            rt = prt;
            prt = prt.parent;
        }
    }
    return false;
}
Game_Character.prototype.moveTowardPoint = function(x, y) {
    var next = this.findNextRouteTo(x, y);
    if (next) {
        var dx = next[0] - this._x;
        var dy = next[1] - this._y;
        var dir = 0;
        if (dy != 0)
            dir = dy < 0 ? 8 : 2;
        else if (dx != 0)
            dir = dx < 0 ? 4 : 6;
        this.moveStraight(dir);
        // stuck?
        if (!this.isMovementSucceeded()) {
            console.log("Move fail, trying to jump");
            this.jump(dx, dy);
        }
    }
    else {
        const sx = this.deltaXFrom(x);
        const sy = this.deltaYFrom(y);
        if (Math.abs(sx) > Math.abs(sy)) {
            this.moveStraight(sx > 0 ? 4 : 6);
            if (!this.isMovementSucceeded() && sy !== 0) {
                this.moveStraight(sy > 0 ? 8 : 2);
            }
        } else if (sy !== 0) {
            this.moveStraight(sy > 0 ? 8 : 2);
            if (!this.isMovementSucceeded() && sx !== 0) {
                this.moveStraight(sx > 0 ? 4 : 6);
            }
        }
    }
};

Array.prototype.shallowClone = function() {
    var re = [];
    for(var i = 0; i < this.length; i++) re.push(this[i]);
    return re;
}

Array.prototype.each = function(f) {
    for(var i = 0; i < this.length; i++) {
        f.apply(this, [this[i], i]);
    }
    return this;
}

Array.prototype.where = function(f) {
    var res = [];
    this.each((x, i) => {
        if (f.apply(this, [x, i])) 
            res.push(x);
    });
    return res;
}

Array.prototype.any = function(f) {
    for(var i = 0; i < this.length; i++) {
        var res = f.apply(this, [this[i], i]);
        if (res) return true;
    }
    return false;
}

Array.prototype.select = function(f) {
    var res = [];
    this.each((x, i) => {
        res.push(f.apply(this, [x, i]));
    });
    return res;
}

Array.prototype.first = function(f, d) {
    if (f) {
        var res = this.where(f)[0];
        if (res.length == 0) return d;
        return res[0];
    }
    if (this.length == 0) return d;
    return this[0];
}

Array.prototype.first = function(f, d) {
    if (f) {
        var res = this.where(f)[0];
        if (res.length == 0) return d;
        return res[0];
    }
    if (this.length == 0) return d;
    return this[0];
}

// Initialize
const mz = new MZHelper();
const cmz = new CMZ();