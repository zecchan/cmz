//=============================================================================
// Coatl RPG Maker MZ - NPC Plugin
//=============================================================================

/*:
 * @target MZ
 * @plugindesc Allows you to move event by schedule.
 * @author Zecchan Silverlake
 *
 * @help cmz.npc.js v1.0.0
 *
 * Require cmz.datetime with RTC enabled.
 * 
 * Tags: GiftHate, GiftDislike, GiftLike, GiftLove
 * 
 * @param cangive
 * @text Allow Gifting to NPCs
 * @type boolean
 * @on Yes
 * @off No
 * @default true
 * 
 * @param gifttext
 * @text Gift Menu Label
 * @type string
 * @default Give
 *
 * @command schedule
 * @text Schedule
 * @desc NPC Route Schedule. Only the first one of each page will be considered. Movement must be set to Fixed.
 *
 * @arg routes
 * @type struct<ScheduleStruct>[]
 * @text Routes
 * @desc The route of this NPC.
 * 
 * @arg hideOffSchedule
 * @text Hide NPC
 * @type boolean
 * @on Yes
 * @off No
 * @desc When turned on, NPC will be hidden when off-schedule.
 * @default false
 *
 * @command giftable
 * @text Giftable
 * @desc Makes this NPC giftable. This page must be set to autorun and placed on the rightmost.
 * 
 * @arg tag
 * @type string
 * @text Tag
 * @desc The tag to process the likeability of gift.
 * 
 * @arg varid
 * @type variable
 * @text Likeability Variable
 * @desc The variable to store the likeability value. Values: 1=Hate, 2=Dislike, 3=Neutral, 4=Like, 5=Love, 0=Cancel
 * 
 * @arg itvarid
 * @type variable
 * @text Item Type Variable
 * @desc The variable to store the item type of the gift. Values: 1=item, 2=keyitem, 3=weapon, 4=armor, 0=Cancel
 * 
 * @arg iivarid
 * @type variable
 * @text Item ID Variable
 * @desc The variable to store the item id. 0=Cancel
 */

/*~struct~ScheduleStruct:
 *
 * @param start
 * @text Start Time
 * @type struct<TimeSpan>
 * @desc The starting time of this phase
 * @default
 * 
 * @param end
 * @text End Time
 * @type struct<TimeSpan>
 * @desc The end time of this phase
 * @default
 * 
 * @param origin
 * @text Origin
 * @type struct<DestinationStruct>
 * @desc The origin location of this NPC
 * @default
 * 
 * @param destination
 * @text Destination
 * @type struct<DestinationStruct>
 * @desc The destination location of this NPC
 * @default
 * 
 * @param finalFacing
 * @text Final Facing
 * @type string
 * @desc What direction should the NPC face when schedule finished. (L|T|R|B)
 * @default
 * 
 * @param onarriveswitch
 * @text On Arrive Set Switch To...
 * @type struct<SetSwitchStruct>
 * @desc Set a certain switch when the NPC arrives at destination.
 * @default
 * 
 * @param onarrivevariable
 * @text On Arrive Set Variable To...
 * @type struct<SetVariableStruct>
 * @desc Set a certain variable when the NPC arrives at destination.
 * @default
 * 
 * @param onarriveselfswitch
 * @text On Arrive Set Self Switch To...
 * @type struct<SetSelfSwitchStruct>
 * @desc Set a self switch when the NPC arrives at destination.
 * @default
 */

/*~struct~SetVariableStruct:
 * @param variable
 * @text Variable
 * @type variable
 * @desc The variable to change
 * @default 0
 * 
 * @param value
 * @text Value
 * @type number
 * @desc The value of variable
 * @default 0
 */

/*~struct~SetSwitchStruct:
 * @param switch
 * @text Switch
 * @type switch
 * @desc The switch to change
 * @default 0
 * 
 * @param value
 * @text Value
 * @type boolean
 * @desc The value of switch
 * @default true
 */

/*~struct~SetSelfSwitchStruct:
 * @param switch
 * @text Self-Switch
 * @type string
 * @desc The self-switch to change
 * @default A
 * 
 * @param value
 * @text Value
 * @type boolean
 * @desc The value of switch
 * @default true
 */

/*~struct~DestinationStruct:
 *
 * @param x
 * @text X
 * @type number
 * @desc X point of map
 * @default 0
 * 
 * @param y
 * @text Y
 * @type number
 * @desc Y point of map
 * @default 0
 */

/*~struct~TimeSpan:
 *
 * @param hour
 * @text Hour
 * @type number
 * @desc The hour value (24-hours)
 * @default 0
 * 
 * @param minute
 * @text Minute
 * @type number
 * @desc The minute value
 * @default 0
 * 
 * @param second
 * @text Second
 * @type number
 * @desc The second value
 * @default 0
 */