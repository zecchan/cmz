Game_Event.prototype.__cmzInitFlag = false;
cmz.events.onGameEventRefresh = new CMZFunctionHook(Game_Event, 'refresh');

class CMZNPC extends CMZPluginBase {
    _meta = {
        name : "CMZ NPC",
        id : "npc",
        author : "Code Atelier",
        version : new Version(1, 0, 0),
        cmzMinVersion : new Version(1, 0 ,0),
        require : [
            {
                id: "datetime",
                version: new Version(1, 0, 0)
            }
        ],
    }

    _giftingTag = null;
    _giftingVar = null;
    _giftingITVar = null;
    _giftingIIVar = null;
    _giftingEventId = null;
    _giftingEventDir = null;
    _giftedEventId = null;

    data = {
    };

    extractScheduleFromEvent(ev) {
        try{
            if (!ev || ev._moveType != 0 || !ev.page()) 
                return;
            }
        catch{ return; }
        var list = ev.list();
        var schedule = null;
        for(var w = 0; w < list.length; w++) {
            var l = list[w];
            if (l.code == 357 && l.parameters[0] == "cmz.npc" && l.parameters[1] == "schedule") {
                schedule = l.parameters[3];
                break;
            }
        }

        if (schedule) {
            schedule.routes = mz.sanitizePluginParameter(schedule.routes);
        }

        return schedule;
    }

    findCurrentRoute(routes) {
        if(!Array.isArray(routes)) return null;
        for(var w = 0; w < routes.length; w++) {
            for(var w = 0; w < routes.length; w++) {
                var r = routes[w];
                var st = "T" + r.start.hour + ":" + r.start.minute + ":" + r.start.second;
                var ed = "T" + r.end.hour + ":" + r.end.minute + ":" + r.end.second;
                if (cmz.plugins.datetime.clock.between(st,ed)) {
                    return r;
                }
            }
        }
        return null;
    }

    findPreviousRoute(routes) {
        if(!Array.isArray(routes)) return null;
        var maxR = null;
        var maxT = 0;
        for(var w = 0; w < routes.length; w++) {
            for(var w = 0; w < routes.length; w++) {
                var r = routes[w];
                var ed = "T" + r.end.hour + ":" + r.end.minute + ":" + r.end.second;
                if (cmz.plugins.datetime.clock.after(ed)) {
                    var val = r.end.hour * 100000 + r.end.minute * 100 + r.end.second;
                    if (val > maxT)
                    {
                        maxR = r;
                        maxT = val;
                    }
                }
            }
        }
        return maxR;
    }

    findNextRoute(routes) {
        if(!Array.isArray(routes)) return null;
        var minR = null;
        var minT = 0;
        for(var w = 0; w < routes.length; w++) {
            for(var w = 0; w < routes.length; w++) {
                var r = routes[w];
                var ed = "T" + r.end.hour + ":" + r.end.minute + ":" + r.end.second;
                if (cmz.plugins.datetime.clock.before(ed)) {
                    var val = r.end.hour * 100000 + r.end.minute * 100 + r.end.second;
                    if (val > minT)
                    {
                        minR = r;
                        minT = val;
                    }
                }
            }
        }
        return minR;
    }

    hide(schedule, event) {
        if (schedule.hideOffSchedule) event.setOpacity(0);
    }

    calculateSpawn(r) {
        var origin = r.origin;
        var dest = r.destination;
        var st = cmz.plugins.datetime.clock.parseNormalize("T" + r.start.hour + ":" + r.start.minute + ":" + r.start.second).toTime();
        var ed = cmz.plugins.datetime.clock.parseNormalize("T" + r.end.hour + ":" + r.end.minute + ":" + r.end.second).toTime();
        var cr = cmz.plugins.datetime.clock.toTime();
        var delta = ed - st;
        var prog = cr - st;
        var perc = prog / delta;

        var x = Math.round((dest.x - origin.x) * perc) + origin.x;
        var y = Math.round((dest.y - origin.y) * perc) + origin.y;
        return {
            x: x,
            y: y
        };
    }

    initialize() {
        cmz.events.onTimeChanged.after(function() {
            if ($gameMap) {
                for(var i = 1; i < $gameMap._events.length; i++) {
                    var ev = $gameMap._events[i];
                    var schedule = cmz.plugins.npc.extractScheduleFromEvent(ev);

                    if (!schedule) continue;
                    var routes = schedule.routes;
                    if(!Array.isArray(routes)) {
                        cmz.plugins.npc.hide(schedule, ev);
                        continue;
                    }
                    var r = cmz.plugins.npc.findCurrentRoute(routes);
                    if (!r) {
                        cmz.plugins.npc.hide(schedule, ev);
                        ev.__lastRoute = undefined;
                        continue;
                    }

                    if (ev.__lastRoute !== r) {
                        ev.__lastRoute = r;

                        // spawn now
                        ev.setOpacity(255);
                        var spwn = cmz.plugins.npc.calculateSpawn(r);
                        ev.locate(spwn.x, spwn.y);
                    }

                    if (ev.x != r.destination.x || ev.y != r.destination.y) {
                        if (!ev.isMoving())
                            ev.moveTowardPoint(r.destination.x, r.destination.y);
                    } else {
                        // arrived
                        if (!ev.isMoving()) {
                            var ff = (r.finalFacing || "").toLowerCase();
                            if (ff) {
                                var dir = ff == "l" ? 4 : ff == "t" ? 8 : ff == "r" ? 6 : 2;
                                if (ev._direction != dir) {
                                    ev.setDirection(dir);
                                }
                            }
                            if (r.onarriveswitch && r.onarriveswitch.switch > 0) {
                                $gameSwitches.setValue(r.onarriveswitch.switch, r.onarriveswitch.value);
                            }
                            if (r.onarrivevariable && r.onarrivevariable.variable > 0) {
                                $gameVariables.setValue(r.onarrivevariable.variable, r.onarrivevariable.value);
                            }
                        }
                    }
                }
            }
        });

        var params = mz.pluginParameter("cmz.npc");
        
        if (params.cangive) {
            cmz.events.onAddMenuCustomCommand.after(function() {
                cmz.plugins.npc._giftingEventId = null;
                cmz.plugins.npc._giftingEventDir = null;
                cmz.plugins.npc._giftingTag = null;
                cmz.plugins.npc._giftingVar = null;
                cmz.plugins.npc._giftingITVar = null;
                cmz.plugins.npc._giftingIIVar = null;

                // scan NPCs
                var evts = $gameMap?.events() || [];
                for(var i = 0; i < evts.length; i++) {
                    var evt = evts[i];
                    if (!evt) continue;

                    var x = evt._x;
                    var y = evt._y;
                    var pd = $gamePlayer._direction;
                    var px = $gamePlayer._x;
                    var py = $gamePlayer._y;
                    var dx = px + (pd == 6 ? 1 : pd == 4 ? -1 : 0);
                    var dy = py + (pd == 2 ? 1 : pd == 8 ? -1 : 0);
                    if (x != dx && y != dy) continue;

                    var dir = evt._direction;
                    evt = $dataMap.events[evt._eventId];
                    var pFound = false;
                    for(var p = 0; p < evt.pages.length; p++) {
                        var page = evt.pages[p];
                        for(var l = 0; l < page.list.length; l++) {
                            var c = page.list[l];
                            if (c.code == 357 && c.parameters[0] == "cmz.npc") {
                                cmz.plugins.npc._giftingEventId = evt.id;
                                cmz.plugins.npc._giftingTag = c.parameters[3].tag;
                                cmz.plugins.npc._giftingVar = Number(c.parameters[3].varid);
                                cmz.plugins.npc._giftingITVar = Number(c.parameters[3].itvarid);
                                cmz.plugins.npc._giftingIIVar = Number(c.parameters[3].iivarid);
                                cmz.plugins.npc._giftingEventDir = dir;
                                pFound = true;
                                break;
                            }
                        }
                        if (pFound) break;
                    }
                }
                this.caller.addCommand(params.gifttext, "npc.gift", cmz.plugins.npc._giftingEventId !== null);
            });
            cmz.events.onCreateCommandWindow.after(function() {
                this.caller._commandWindow.setHandler("npc.gift", () => {
                    SceneManager.push(Scene_ItemGift);
                });
            });
            cmz.events.onCheckEventPage.after(function(page) {
                if (this.lastReturn === false) return false;
        
                for (var i = 0; i < page.list.length; i++) {
                    var c = page.list[i];
                    if (c.code == 357 && c.parameters[0] == "cmz.npc" && c.parameters[1] == "giftable" && cmz.plugins.npc._giftedEventId != this.caller._eventId) {
                        return false;
                    }
                }
        
                return true;
            });

            PluginManager.registerCommand("cmz.npc", "giftable", args => {
                cmz.plugins.npc._giftedEventId = null;
                cmz.plugins.npc._giftingEventId = null;
                cmz.plugins.npc._giftingTag = null;
                cmz.plugins.npc._giftingVar = null;
                cmz.plugins.npc._giftingEventDir = null;
                cmz.plugins.npc._giftingITVar = null;
                cmz.plugins.npc._giftingIIVar = null;
                mz.requestEventRefresh();
            });
        }
    }
}

class Window_ItemGiftList extends Window_ItemList {
    constructor(rect) {
        super(rect);
    }

    isEnabled(item) {
        return true;
    };
}

class Scene_ItemGift extends Scene_Item {
    constructor() {
        super();
    }
    
    createItemWindow() {
        const rect = this.itemWindowRect();
        this._itemWindow = new Window_ItemGiftList(rect);
        this._itemWindow.setHelpWindow(this._helpWindow);
        this._itemWindow.setHandler("ok", this.onItemOk.bind(this));
        this._itemWindow.setHandler("cancel", this.onItemCancel.bind(this));
        this.addWindow(this._itemWindow);
        this._categoryWindow.setItemWindow(this._itemWindow);
        if (!this._categoryWindow.needsSelection()) {
            this._itemWindow.y -= this._categoryWindow.height;
            this._itemWindow.height += this._categoryWindow.height;
            this._categoryWindow.update();
            this._categoryWindow.hide();
            this._categoryWindow.deactivate();
            this.onCategoryOk();
        }
    };

    canUse() {
        return true;
    }

    determineItem() {
        const item = this.item();
        $gameParty.loseItem(item, 1, true);
        cmz.plugins.npc._giftedEventId = cmz.plugins.npc._giftingEventId;
        $gameMap?._events[cmz.plugins.npc._giftedEventId].setDirection(cmz.plugins.npc._giftingEventDir);
        SceneManager.pop();
        SceneManager.pop();
        
        var itemType = item.itypeId == 1 ? 1 : item.itypeId == 2 ? 2 : item.etypeId == 1 ? 3 : item.atypeId == 0 ? 4 : 0;
        var itemId = item.id;

        var like = 3;
        if (item.meta) {
            var pars = ["GiftHate", "GiftDislike", "GiftNormal", "GiftLike", "GiftLove"];
            var found = false;
            for(var i = 0; i < pars.length; i++) {
                var lst = (item.meta[pars[i]] || "").split(",");
                for(var l = 0; l < lst.length; l++) {
                    var tag = lst[l].trim();
                    if (tag == cmz.plugins.npc._giftingTag) {
                        like = i + 1;
                        found = true;
                        break;
                    }
                }
                if (found) break;
            }
        }

        if (cmz.plugins.npc._giftingVar) $gameVariables.setValue(cmz.plugins.npc._giftingVar, like);
        if (cmz.plugins.npc._giftingIIVar) $gameVariables.setValue(cmz.plugins.npc._giftingIIVar, itemId);
        if (cmz.plugins.npc._giftingITVar) $gameVariables.setValue(cmz.plugins.npc._giftingITVar, itemType);

        console.log(cmz.plugins.npc._giftingTag, itemType, itemId, like);
    };
}

cmz.registerPlugin(new CMZNPC());