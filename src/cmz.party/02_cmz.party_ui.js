//-----------------------------------------------------------------------------
// Window_PartyListUI
//
// The window for displaying the party member list

function Window_PartyListUI() {
    this.initialize(...arguments);
};

Window_PartyListUI.prototype = Object.create(Window_Selectable.prototype);
Window_PartyListUI.prototype.constructor = Window_PartyListUI;
Window_PartyListUI.prototype._list = [];
Window_PartyListUI.prototype.maxItems = function() {
    return this._list.length;
};

Window_PartyListUI.prototype.initialize = function (rect, infoWindow) {
    Window_Selectable.prototype.initialize.call(this, rect);
    this._infoWindow = infoWindow;
    this.setHandler("ok", () => {});
    this.refresh();
    this.select(0);
    this.activate();
};

Window_PartyListUI.prototype.refresh = function() {
    this.makeMemberList();
    Window_Selectable.prototype.refresh.call(this);
};

Window_PartyListUI.prototype.clearMemberList = function() {
    this._list = [];
};

Window_PartyListUI.prototype.makeMemberList = function() {
    var memberList = [];
    for(var i = 0; i < cmz.plugins.party.params.memlistdb.length; i++) {
        var mem = cmz.plugins.party.params.memlistdb[i];
        var actorId = mem.actorId;
        if (actorId && (mem.startAvailable || cmz.plugins.party.inviteEnabled(actorId))) {
            var illustPath = cmz.plugins.party.getIllustPath(actorId);
            if (illustPath) {
                ImageManager.loadBitmapFromUrl("img/pictures/" +  illustPath.replace("\\", "/") + ".png");
            }
            var member = {
                actorId: actorId,
                actor: $dataActors[actorId],
                game: $gameActors ? $gameActors._data[actorId] : null,
                member: mem,
                canInvite: cmz.plugins.party.canInvite(actorId),
                inParty: $gameParty ? $gameParty._actors.includes(actorId) : false,
                illustPath: illustPath,
            };
            memberList.push(member);
        }
    }
    this._list = memberList;
}
Window_PartyListUI.prototype.itemAt = function(index) {
    return this._list && index >= 0 ? this._list[index] : null;
};
Window_PartyListUI.prototype.drawItem = function(index) {
    var cur = this.itemAt(index);
    if (!cur) return;
    const rect = this.itemLineRect(index);
    const align = "left";
    if (cur.canInvite) {
        if (cur.inParty) {
            this.changeTextColor(ColorManager.textColor(1));
        } else {
            this.resetTextColor();
        }
    } else {
        this.changeTextColor(ColorManager.textColor(2));
    }
    this.drawText(cur.actor.name, rect.x, rect.y, rect.width, align);
    this.resetTextColor();
};

Window_PartyListUI.prototype.select = function(index) {
    Window_Selectable.prototype.select.call(this, index);
    if (this._infoWindow) {
        this._infoWindow._selected = this.itemAt(index);
        this._infoWindow.resetScroll();
        this._infoWindow.refresh();
    }
};

Window_PartyListUI.prototype.processOk = function() {
    var current = this.itemAt(this._index);
    if (current) {
        if (!cmz.plugins.party._isMenu || cmz.plugins.party.params.caninvitelist) {
            if (current.inParty) {
                SoundManager.playCancel();
                $gameParty.removeActor(current.actorId);
            }
            else if (current.canInvite) {
                if ($gameParty && $gameParty._actors.length >= cmz.plugins.party.params.maxpartymember) {
                    this.playBuzzerSound();
                } else {
                    this.playOkSound();
                    if ($gameActors && !$gameActors._data[current.actorId]) {
                        $gameActors.actor(current.actorId).setup(current.actorId);
                    }
                    $gameParty.addActor(current.actorId);
                }
            } 
            else {
                this.playBuzzerSound();
            } 
        }     
        this.refresh();
        if (this._infoWindow) {
            this._infoWindow._selected = this.itemAt(this._index);
            this._infoWindow.resetScroll();
            this._infoWindow.refresh();
        }
    }
}

//-----------------------------------------------------------------------------
// Window_PartyMemberInfoUI
//
// The window for displaying the party member info

function Window_PartyMemberInfoUI() {
    this.initialize(...arguments);
}

Window_PartyMemberInfoUI.prototype = Object.create(Window_Selectable.prototype);
Window_PartyMemberInfoUI.prototype.constructor = Window_PartyMemberInfoUI;


Window_PartyMemberInfoUI.prototype.initialize = function(rect) {
    Window_Selectable.prototype.initialize.call(this, rect);
    this.refresh();
};

Window_PartyMemberInfoUI.prototype.resetScroll = function() {
    this._scroll = 0;
};

Window_PartyMemberInfoUI.prototype.refresh = function() {
    this.contents.clear();
    var y = 0;
    var rect = this.itemLineRect(0);
    if (this._selected) {
        var q = this._selected;

        this.resetTextColor();
        this.changePaintOpacity(true);
        if (q.canInvite) {
            if (q.inParty) {
                this.changeTextColor(ColorManager.textColor(1));
            } else {
                //this.changeTextColor(ColorManager.textColor(3));
            }
        } else {
            this.changeTextColor(ColorManager.textColor(2));
        }
        var nickname = q.actor.nickname ? " (" + q.actor.nickname + ")" : "";
        this.drawText(q.actor.name + nickname, 0, rect.height * y, rect.width / 2 - 5, "left");
        this.resetTextColor();
        var lv = q.game ? "Lv " + q.game._level : "Lv ?";
        if (q.game) {
            var cls = $dataClasses[q.game._classId];
            lv += " " + cls.name;
        }
        this.drawText(lv, rect.width / 2 + 5, rect.height * y, rect.width / 2 - 5, "left");

        y += 2;

        // draw img
        var imgWidth = rect.width / 3 * 2 - 5;
        var imgHeightBound = this._height - y * rect.height;
        if (q.illustPath) {
            const bmp = ImageManager.loadBitmapFromUrl("img/pictures/" + q.illustPath.replace("\\", "/") + ".png");
            const pw = bmp.width;
            const ph = bmp.height;
            var scl = imgWidth / pw;
            var dw = pw * scl;
            var dh = ph * scl;
            if (dh > imgHeightBound) {
                scl = imgHeightBound / ph;
                dw = pw * scl;
                dh = ph * scl;
            }
            this.contents.blt(bmp, 0, 0, pw, ph, 0, y * rect.height + (imgHeightBound - dh), dw, dh);
        }

        // draw stats
        var statWidth = rect.width / 6 - 5;
        var statOffset = rect.width / 3 * 2 + 5;
        var statValueOffset = statOffset + statWidth + 10;
        this.resetTextColor();

        var stats = ["atk", "def", "mat", "mdf", "agi", "luk"];
        for(var i = 0; i < stats.length; i++) {
            var stat = stats[i];
            this.drawText(stat.toUpperCase(), statOffset, rect.height * y, statWidth);
            this.drawText(q.game ? q.game[stat] : "???", statValueOffset, rect.height * y, statWidth, "right");
            y++;
        }
        y++;

        // custom stats
        var customStats = q.member.customstats;
        if (customStats && Array.isArray(customStats)) {
            for(var i = 0; i < customStats.length; i++) {
                var stat = customStats[i];
                this.drawText(stat.name, statOffset, rect.height * y, statWidth);
                var value = stat.evalValue? cmz.eval.execValue("" + stat.evalValue) : "???";
                this.drawText(value, statValueOffset, rect.height * y, statWidth, "right");
                y++;
            }
        }
    }
};

//-----------------------------------------------------------------------------
// Scene_PartyMemberListUI
//
// The scene for displaying the party member list
function Scene_PartyMemberListUI() {
    this.initialize(...arguments);
}

Scene_PartyMemberListUI.prototype = Object.create(Scene_Base.prototype);
Scene_PartyMemberListUI.prototype.constructor = Scene_PartyMemberListUI;

Scene_PartyMemberListUI.prototype.initialize = function() {
    Scene_Base.prototype.initialize.call(this);
};

Scene_PartyMemberListUI.prototype.create = function() {
    Scene_Base.prototype.create.call(this);
    this.createBackground();
    this.createWindowLayer();
    this.createMemberInfoWindow();
    this.createPartyListWindow();
};

Scene_PartyMemberListUI.prototype.createBackground = function() {
    this._backgroundFilter = new PIXI.filters.BlurFilter();
    this._backgroundSprite = new Sprite();
    this._backgroundSprite.bitmap = SceneManager.backgroundBitmap();
    this._backgroundSprite.filters = [this._backgroundFilter];
    this.addChild(this._backgroundSprite);
    this.setBackgroundOpacity(192);
};

Scene_PartyMemberListUI.prototype.setBackgroundOpacity = function(opacity) {
    this._backgroundSprite.opacity = opacity;
};

Scene_PartyMemberListUI.prototype.createPartyListWindow = function() {
    const rect = this.partyListWindowRect();
    const window = new Window_PartyListUI(rect, this._memberInfoWindow);
    this.addWindow(window);
    window.setHandler("cancel", this.popScene.bind(this));
    this._partyListWindow = window;
};

Scene_PartyMemberListUI.prototype.partyListWindowRect = function() {
    const ww = Graphics.boxWidth / 3 - 5;
    const wh = Graphics.boxHeight;
    const wx = 0;
    const wy = 0;
    return new Rectangle(wx, wy, ww, wh);
};

Scene_PartyMemberListUI.prototype.createMemberInfoWindow = function() {
    const rect = this.memberInfoWindowRect();
    const window = new Window_PartyMemberInfoUI(rect);
    this.addWindow(window);
    this._memberInfoWindow = window;
};

Scene_PartyMemberListUI.prototype.memberInfoWindowRect = function() {
    const ww = Graphics.boxWidth / 3 * 2 - 5;
    const wh = Graphics.boxHeight;
    const wx = this.partyListWindowRect().width + 10;
    const wy = 0;
    return new Rectangle(wx, wy, ww, wh);
};