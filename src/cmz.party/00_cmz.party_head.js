//=============================================================================
// Coatl RPG Maker MZ - NPC Plugin
//=============================================================================

/*:
 * @target MZ
 * @plugindesc Allows you to move event by schedule.
 * @author Zecchan Silverlake
 *
 * @help cmz.npc.js v1.0.0
 *
 * Require cmz.core
 * 
 * @param showmemlist
 * @text Show Member List in Menu
 * @type boolean
 * @on Yes
 * @off No
 * @desc Show member list in game menu
 * @default true
 * 
 * @param memlistname
 * @text Member List Name
 * @type string
 * @desc Display name of member list
 * @default Party Members
 * 
 * @param caninvitelist
 * @text Can Invite Member From Menu List
 * @type boolean
 * @on Yes
 * @off No
 * @desc Allow player to invite party member from member list in menu
 * @default true
 * 
 * @param maxpartymember
 * @text Max Party Members
 * @type number
 * @desc Maximum number of characters in a party
 * @default 4
 * 
 * @param memlistdb
 * @text Member Database
 * @type struct<MemberStruct>[]
 * @default []
 * 
 * @command showlist
 * @text Show Member List
 * @desc Show the member list scene
 * 
 * @arg canInvite
 * @text Can invite
 * @type boolean
 * @on Allow
 * @off Disallow
 * @desc Set whether inviting the actor to party is allowed or not.
 * @default true
 * 
 * @command caninvite
 * @text Set Invite Actor to Party
 * @desc Makes an actor invitable or not.
 * 
 * @arg actorId
 * @type actor
 * @text Actor
 * @desc The actor that will be shown in member list
 * 
 * @arg value
 * @text Allow Invite?
 * @type boolean
 * @on Allow
 * @off Disallow
 * @desc Set whether inviting the actor to party is allowed or not.
 * @default true
 */


/*~struct~MemberStruct:
 *
 * @param actorId
 * @text Actor
 * @type actor
 * @desc The actor that will be shown in member list
 * @default 0
 * 
 * @param startAvailable
 * @text Available From Start
 * @type boolean
 * @on Yes
 * @off No
 * @desc Set whether the actor is available to invite from the start
 * @default false
 * 
 * @param illustPath
 * @text Illustration Path
 * @type string
 * @desc Illustration path, relative to img/picture
 * @default
 * 
 * @param conditionalIllust
 * @text Conditional Illust
 * @type struct<ConditionalIllust>[]
 * @default []
 * 
 * @param customstats
 * @text Custom Stats
 * @type struct<CustomMemberStat>[]
 * @default []
 */

/*~struct~ConditionalIllust:
 *
 * @param illustPath
 * @text Illustration Path
 * @type string
 * @desc Illustration path, relative to img/picture
 * @default
 * 
 * @param evalValue
 * @text Eval Value
 * @type string
 * @desc JavaScript delta function to evaluate the boolean value. Compatible with cmz eval.
 * @default
 */

/*~struct~CustomMemberStat:
 *
 * @param name
 * @text Stat Name
 * @type string
 * @desc The stat name
 * @default
 * 
 * @param evalValue
 * @text Eval Value
 * @type string
 * @desc JavaScript delta function to evaluate the value. Compatible with cmz eval.
 * @default
 */