class CMZParty extends CMZPluginBase {
    constructor() {
        super();
        this.params = mz.pluginParameter("cmz.party");
    }

    initialize() {
        if (this.params.showmemlist) {
            cmz.runOnce("party.memberlist", function() {
                cmz.events.onAddMenuCustomCommand.after(function() {
                    this.caller.addCommand(cmz.plugins.party.params.memlistname, "party.memberlist", cmz.plugins.party.params.showmemlist);
                });
            });
            cmz.events.onCreateCommandWindow.after(function() {
                this.caller._commandWindow.setHandler("party.memberlist", () => {
                    cmz.plugins.party._isMenu = true;
                    SceneManager.push(Scene_PartyMemberListUI);
                });
            });
        }
    }

    _meta = {
        name : "CMZ Party",
        id : "party",
        author : "Code Atelier",
        version : new Version(1, 0, 0),
        cmzMinVersion : new Version(1, 0 ,0),
        require : [
        ],
    }

    data = {
        disableAllInvites: false,
        disableInvite: [],
        canInvite: [],
        customIllust: {},
    };

    getIllustPath(actorId) {
        var mem = this.getMemberByActorId(actorId);
        if (mem) {
            var illPath = this.data.customIllust['mem' + actorId];
            if (!illPath && mem.conditionalIllust) {
                for(var i = 0; i < mem.conditionalIllust.length; i++) {
                    var ci = mem.conditionalIllust[i];
                    var value = ci.evalValue ? cmz.eval.exec("" + ci.evalValue) : false;
                    if (value) {
                        illPath = ci.illustPath;
                        break;
                    }
                }
            }
            if (!illPath) {
                illPath = mem.illustPath;
            }
            if (illPath) {
                return illPath;
            }
        }
        return null;
    }

    getMemberByActorId(actorId) {
        for(var i = 0; i < this.params.memlistdb.length; i++) {
            var ml = this.params.memlistdb[i];
            if (ml.actorId == actorId)
                return ml;
        }
        return null;
    }

    inviteEnabled(actorId) {
        return this.data.canInvite.includes(actorId);
    }

    inviteDisabled(actorId) {
        return this.data.disableInvite.includes(actorId);
    }

    canInvite(actorId) {
        var ml = this.getMemberByActorId(actorId);
        return ml && (ml.startAvailable || this.inviteEnabled(actorId)) && !this.inviteDisabled(actorId) && !this.data.disableAllInvites;
    }

    showMemberList(canInvite) {
        cmz.plugins.party._isMenu = canInvite ? false : true;
        SceneManager.push(Scene_PartyMemberListUI);
    }

    setInvitable(actorId, value) {
        if (value) {
            if (!this.data.canInvite.contains(actorId)) {
                this.data.canInvite.push(actorId);
            }
        } else {
            if (this.data.canInvite.contains(actorId)) {
                this.data.canInvite.remove(actorId);
            }
        }
    }
}

// Register to plugin manager
PluginManager.registerCommand("cmz.party", "showlist", args => {
    var arg = mz.sanitizePluginParameter(args);
    cmz.plugins.party.showMemberList(arg.canInvite);
});
PluginManager.registerCommand("cmz.party", "caninvite", args => {
    var arg = mz.sanitizePluginParameter(args);
    cmz.plugins.party.setInvitable(arg.actorId, arg.value);
});

cmz.registerPlugin(new CMZParty());