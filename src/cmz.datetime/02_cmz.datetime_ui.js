if (mz.pluginParameter("cmz.datetime").useUI) {

    //-----------------------------------------------------------------------------
    // Window_DateTimeUI
    //
    // The window for displaying the map name on the map screen.

    function Window_DateTimeUI() {
        this.initialize(...arguments);
    }

    Window_DateTimeUI.prototype = Object.create(Window_Base.prototype);
    Window_DateTimeUI.prototype.constructor = Window_DateTimeUI;

    Window_DateTimeUI.prototype.initialize = function (rect) {
        Window_Base.prototype.initialize.call(this, rect);
        this.opacity = 0;
        this.refresh();
    };

    Window_DateTimeUI.prototype.refresh = function () {
        this.contents.clear();
        if (mz.isEventRunning()) return;
        var m = cmz.plugins.datetime.clock.date.month;
        if (cmz.plugins.datetime.struct.months.length != 4) {
            m = Math.ceil(m / cmz.plugins.datetime.struct.months.length * 4);
        }
        const bitmap = ImageManager.loadSystem(
            m == 2 ? "s02_summer" :
            m == 3 ? "s03_fall" :
            m == 4 ? "s04_winter" :
            "s01_spring"
            );
        const pw = 228;
        const ph = 106;
        const sx = 1;
        const sy = 1;
        this.contents.blt(bitmap, sx, sy, pw, ph, 0, 0);

        var ofs = this.contents.fontSize;
        this.contents.fontSize = 32;
        var prm = mz.pluginParameter("cmz.datetime");
        this.contents.drawText(cmz.plugins.datetime.clock.toString(prm.uiTimeFormat || "ddd hh:mm N"), 60, 10, 160, this.lineHeight(), "left");
        this.contents.drawText(cmz.plugins.datetime.clock.toString(prm.uiDateFormat || "MMM dd, yyyy"), 0, 60, 228, this.lineHeight(), "center");
        this.contents.fontSize = ofs;
    };

    Scene_Map.prototype.updateDateTimeUIWindow = function() {
        this._dateTimeUIWindow.refresh();
    };

    Scene_Map.prototype.dateTimeUIWindowRect = function() {
        const wx = 0;
        const wy = 0;
        const ww = 252;
        const wh = 130;
        return new Rectangle(wx, wy, ww, wh);
    };

    Scene_Map.prototype.createDateTimeUIWindow = function() {
        const rect = this.dateTimeUIWindowRect();
        this._dateTimeUIWindow = new Window_DateTimeUI(rect);
        this.addWindow(this._dateTimeUIWindow);
    };

    var original_SceneMapcreateallWindows = Scene_Map.prototype.createAllWindows;
    Scene_Map.prototype.createAllWindows = function() {
        original_SceneMapcreateallWindows.call(this);   
        this.createDateTimeUIWindow();     
    };

    var original_SceneMapupdate = Scene_Map.prototype.update;
    Scene_Map.prototype.update = function() {
        original_SceneMapupdate.call(this); 
        this.updateDateTimeUIWindow();    
    }
}