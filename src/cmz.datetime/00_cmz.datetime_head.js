//=============================================================================
// Coatl RPG Maker MZ - DateTime Plugin
//=============================================================================

/*:
 * @target MZ
 * @plugindesc Allows you to implement date time in-game.
 * @author Zecchan Silverlake
 *
 * @help cmz.datetime.js v1.0.0
 *
 * This plugin contains necessary logic for CMZ Plugins.
 * 
 * @param useUI
 * @text Use UI
 * @type boolean
 * @on Yes
 * @off No
 * @desc Use UI to show DateTime
 * @default false
 * 
 * @param uiTimeFormat
 * @text UI Time Format
 * @type string
 * @desc The format of time
 * @default ddd hh:mm N
 * 
 * @param uiDateFormat
 * @text UI Date Format
 * @type string
 * @desc The format of date
 * @default MMM dd, yyyy
 * 
 * @param useRTC
 * @text Use Real Time Clock
 * @type boolean
 * @on Yes
 * @off No
 * @desc Use Real Time Clock for this datetime
 * @default false
 * 
 * @param realSecondToGameSecond
 * @text Time Speed
 * @type number
 * @desc How many second elapsed in game for each second elapsed in real time
 * @default 60
 * 
 * @param usePhase
 * @text Use Phase
 * @type boolean
 * @on Yes
 * @off No
 * @desc Use Phase of Day instead of regular clock. This overrides RTC.
 * @default false
 * 
 * @param initMaxAP
 * @text Initial Max Action Point
 * @type number
 * @desc Initial maximum Action Point per phase.
 * @default 3
 * 
 * @param phases
 * @text Phases
 * @type struct<PhaseStruct>[]
 * @desc List of phases of day, in order.
 * @default ["{\"longname\":\"Early-Morning\",\"shortname\":\"EMr\",\"indoortint\":\"{\\\"red\\\":\\\"-20\\\",\\\"green\\\":\\\"-20\\\",\\\"blue\\\":\\\"0\\\",\\\"gray\\\":\\\"30\\\"}\",\"outdoortint\":\"{\\\"red\\\":\\\"-40\\\",\\\"green\\\":\\\"-40\\\",\\\"blue\\\":\\\"0\\\",\\\"gray\\\":\\\"48\\\"}\",\"start\":\"{\\\"hour\\\":\\\"0\\\",\\\"minute\\\":\\\"0\\\"}\",\"end\":\"{\\\"hour\\\":\\\"5\\\",\\\"minute\\\":\\\"0\\\"}\"}","{\"longname\":\"Morning\",\"shortname\":\"Mor\",\"indoortint\":\"{\\\"red\\\":\\\"-10\\\",\\\"green\\\":\\\"-10\\\",\\\"blue\\\":\\\"0\\\",\\\"gray\\\":\\\"15\\\"}\",\"outdoortint\":\"{\\\"red\\\":\\\"-20\\\",\\\"green\\\":\\\"-20\\\",\\\"blue\\\":\\\"0\\\",\\\"gray\\\":\\\"24\\\"}\",\"start\":\"{\\\"hour\\\":\\\"5\\\",\\\"minute\\\":\\\"0\\\"}\",\"end\":\"{\\\"hour\\\":\\\"9\\\",\\\"minute\\\":\\\"0\\\"}\"}","{\"longname\":\"Noon\",\"shortname\":\"Non\",\"indoortint\":\"{\\\"red\\\":\\\"0\\\",\\\"green\\\":\\\"0\\\",\\\"blue\\\":\\\"0\\\",\\\"gray\\\":\\\"0\\\"}\",\"outdoortint\":\"{\\\"red\\\":\\\"0\\\",\\\"green\\\":\\\"0\\\",\\\"blue\\\":\\\"0\\\",\\\"gray\\\":\\\"0\\\"}\",\"start\":\"{\\\"hour\\\":\\\"9\\\",\\\"minute\\\":\\\"0\\\"}\",\"end\":\"{\\\"hour\\\":\\\"17\\\",\\\"minute\\\":\\\"0\\\"}\"}","{\"longname\":\"Evening\",\"shortname\":\"Eve\",\"indoortint\":\"{\\\"red\\\":\\\"20\\\",\\\"green\\\":\\\"-8\\\",\\\"blue\\\":\\\"-8\\\",\\\"gray\\\":\\\"0\\\"}\",\"outdoortint\":\"{\\\"red\\\":\\\"48\\\",\\\"green\\\":\\\"-16\\\",\\\"blue\\\":\\\"-16\\\",\\\"gray\\\":\\\"0\\\"}\",\"start\":\"{\\\"hour\\\":\\\"17\\\",\\\"minute\\\":\\\"0\\\"}\",\"end\":\"{\\\"hour\\\":\\\"19\\\",\\\"minute\\\":\\\"0\\\"}\"}","{\"longname\":\"Night\",\"shortname\":\"Nig\",\"indoortint\":\"{\\\"red\\\":\\\"-10\\\",\\\"green\\\":\\\"-10\\\",\\\"blue\\\":\\\"-20\\\",\\\"gray\\\":\\\"30\\\"}\",\"outdoortint\":\"{\\\"red\\\":\\\"-40\\\",\\\"green\\\":\\\"-40\\\",\\\"blue\\\":\\\"0\\\",\\\"gray\\\":\\\"48\\\"}\",\"start\":\"{\\\"hour\\\":\\\"19\\\",\\\"minute\\\":\\\"0\\\"}\",\"end\":\"{\\\"hour\\\":\\\"24\\\",\\\"minute\\\":\\\"0\\\"}\"}"]
 * 
 * @param daysinweek
 * @text Days in Week
 * @type struct<DiWStruct>[]
 * @desc List of days in week, in order.
 * @default ["{\"longname\":\"Monday\",\"shortname\":\"Mon\"}","{\"longname\":\"Tuesday\",\"shortname\":\"Tue\"}","{\"longname\":\"Wednesday\",\"shortname\":\"Wed\"}","{\"longname\":\"Thursday\",\"shortname\":\"Thu\"}","{\"longname\":\"Friday\",\"shortname\":\"Fri\"}","{\"longname\":\"Saturday\",\"shortname\":\"Sat\"}","{\"longname\":\"Sunday\",\"shortname\":\"Sun\"}"]
 * 
 * @param months
 * @text Months
 * @type struct<MonthStruct>[]
 * @desc List of months in a year, in order.
 * @default ["{\"longname\":\"Spring\",\"shortname\":\"Spr\",\"daysinmonth\":\"30\",\"rainchance\":\"20\",\"snowchance\":\"0\",\"weatherseverity\":\"20\"}","{\"longname\":\"Summer\",\"shortname\":\"Sum\",\"daysinmonth\":\"30\",\"rainchance\":\"30\",\"snowchance\":\"0\",\"weatherseverity\":\"50\"}","{\"longname\":\"Autumn\",\"shortname\":\"Aut\",\"daysinmonth\":\"30\",\"rainchance\":\"35\",\"snowchance\":\"0\",\"weatherseverity\":\"20\"}","{\"longname\":\"Winter\",\"shortname\":\"Win\",\"daysinmonth\":\"30\",\"rainchance\":\"5\",\"snowchance\":\"30\",\"weatherseverity\":\"40\"}"]
 * 
 * @param phaseChangeSwitches
 * @text On Phase Change, Set Switches To...
 * @type struct<SetSwitchStruct>[]
 * @desc When the phase changed, set the following switches.
 * @default []
 * 
 * @param phaseChangeVariables
 * @text On Phase Change, Set Variables To...
 * @type struct<SetVariableStruct>[]
 * @desc When the phase changed, set the following variables.
 * @default []
 * 
 * @param phaseChangeCommonEvents
 * @text On Phase Change, Run Common Event...
 * @type struct<QueueCommonEventStruct>[]
 * @desc When the phase changed, run the following common events.
 * @default []
 * 
 * @param dayChangeSwitches
 * @text On Day Change, Set Switches To...
 * @type struct<SetSwitchStruct>[]
 * @desc When the day changed, set the following switches.
 * @default []
 * 
 * @param dayChangeVariables
 * @text On Day Change, Set Variables To...
 * @type struct<SetVariableStruct>[]
 * @desc When the day changed, set the following variables.
 * @default []
 * 
 * @command setTime
 * @text Set Time
 * @desc Set the current time. This will not trigger timeChanged event.
 *
 * @arg value
 * @type string
 * @text Value
 * @desc Time value expressed in: yyyy-MM-ddTHH:mm:ss
 * @default 2021-01-01T00:00:00
 * 
 * @command addTime
 * @text Add Time
 * @desc Add to the current time. This will trigger timeChanged event.
 *
 * @arg value
 * @type string
 * @text Value
 * @desc Time value expressed in: yyyy-MM-ddTHH:mm:ss
 * @default 0-0-0T1:0:0
 * 
 * @command subtractTime
 * @text Subtract Time
 * @desc Subtract from the current time. This will trigger timeChanged event.
 *
 * @arg value
 * @type string
 * @text Value
 * @desc Time value expressed in: yyyy-MM-ddTHH:mm:ss
 * @default 0-0-0T1:0:0
 * 
 * @command consumeAP
 * @text Consume AP
 * @desc Consume Action Point
 *
 * @arg value
 * @type number
 * @text AP Amount
 * @desc The number of AP to consume
 * @default 1
 * 
 * @command nextPhase
 * @text Next Phase
 * @desc Skip to next phase of day (only for non-RTC and must use phase)
 * 
 * @command nextDay
 * @text Next Day
 * @desc Skip to next day (only for non-RTC and must use phase)
 *
 * @arg phase
 * @type number
 * @text Phase
 * @desc Skip to phase
 * @default 1
 */

/*~struct~QueueCommonEventStruct:
 * @param commonevent
 * @text Common Event
 * @type number
 * @desc The common event id to run
 * @default 0
 * 
 * @param phase
 * @text Phase
 * @type number
 * @desc Only run on certain phase of day. Blank = run always
 * @default
 * 
 * @param cond
 * @type multiline_string
 * @text Condition
 * @desc Only set the variable when the condition is true. When blank this is ignored.
 */

/*~struct~SetVariableStruct:
 * @param variable
 * @text Variable
 * @type variable
 * @desc The variable to change
 * @default 0
 * 
 * @param value
 * @text Value
 * @type number
 * @desc The value of variable
 * @default 0
 * 
 * @param cond
 * @type multiline_string
 * @text Condition
 * @desc Only set the variable when the condition is true. When blank this is ignored.
 */

/*~struct~SetSwitchStruct:
 * @param switch
 * @text Switch
 * @type switch
 * @desc The switch to change
 * @default 0
 * 
 * @param value
 * @text Value
 * @type boolean
 * @desc The value of switch
 * @default true
 * 
 * @param cond
 * @type multiline_string
 * @text Condition
 * @desc Only set the switch when the condition is true. When blank this is ignored.
 */

/*~struct~PhaseStruct:
 *
 * @param longname
 * @text Long Name
 * @type string
 * @desc The long name of the month
 * @default
 * 
 * @param shortname
 * @text Short Name
 * @type string
 * @desc The short name of the month
 * @default
 * 
 * @param indoortint
 * @text Indoor Tint
 * @type struct<Tint>
 * @desc The tint of indoor map
 * @default
 * 
 * @param outdoortint
 * @text Outdoor Tint
 * @type struct<Tint>
 * @desc The tint of outdoor map
 * @default
 * 
 * @param start
 * @text Start Time
 * @type struct<TimeSpan>
 * @desc The starting time of this phase
 * @default
 * 
 * @param end
 * @text End Time
 * @type struct<TimeSpan>
 * @desc The end time of this phase
 * @default
 */

/*~struct~TimeSpan:
 *
 * @param hour
 * @text Hour
 * @type number
 * @desc The hour value (24-hours)
 * @default 0
 * 
 * @param minute
 * @text Minute
 * @type number
 * @desc The minute value
 * @default 0
 */

/*~struct~Tint:
 *
 * @param red
 * @text Red
 * @type number
 * @desc The value of red tint (-255 ~ 255)
 * @default 0
 * 
 * @param green
 * @text Green
 * @type number
 * @desc The value of green tint (-255 ~ 255)
 * @default 0
 * 
 * @param blue
 * @text Blue
 * @type number
 * @desc The value of blue tint (-255 ~ 255)
 * @default 0
 * 
 * @param gray
 * @text Gray
 * @type number
 * @desc The value of gray tint (0 ~ 255)
 * @default 0
 */

/*~struct~DiWStruct:
 *
 * @param longname
 * @text Long Name
 * @type string
 * @desc The long name of the month
 * @default
 * 
 * @param shortname
 * @text Short Name
 * @type string
 * @desc The short name of the month
 * @default
 */

/*~struct~MonthStruct:
 *
 * @param longname
 * @text Long Name
 * @type string
 * @desc The long name of the month
 * @default
 * 
 * @param shortname
 * @text Short Name
 * @type string
 * @desc The short name of the month
 * @default
 * 
 * @param daysinmonth
 * @text Days in Month
 * @type number
 * @desc How many days in this month
 * @default 30
 * 
 * @param rainchance
 * @text Chance of Rain
 * @type number
 * @desc Chance of rain in percent
 * @default 0
 * 
 * @param snowchance
 * @text Chance of Snow
 * @type number
 * @desc Chance of snow in percent
 * @default 0
 * 
 * @param weatherseverity
 * @text Severity
 * @type number
 * @desc Severity of weathers
 * @default 0
 * 
 */