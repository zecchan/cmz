class CMZDateTime extends CMZPluginBase {
    _meta = {
        name : "CMZ DateTime",
        id : "datetime",
        author : "Code Atelier",
        version : new Version(1, 0 ,0),
        cmzMinVersion : new Version(1, 1, 0),
        require : [],
    }

    data = {
        time: 0,
        maxAP: 0,
        currentWeather: "none",
        currentWeatherSeverity: 0,
        currentWeatherTintMod: 0,
    };

    clock = new CMZDateTimeValue();
    struct = new CMZDateTimeStruct();

    updateTint() {
        var finalTint = [0,0,0,0];
        if ($dataMap) {
            var cPhase = this.struct.phase(this.clock);
            var phase = this.struct.phases[cPhase];

            if ($dataMap.meta.tint) {
                var spl = $dataMap.meta.tint.split(/, */);
                if (spl.length == 4) {
                    for(var i = 0; i < 4; i++) {
                        finalTint[i] = Number(spl[i]) || 0;
                    }
                }
            } else if (phase && $dataMap.meta.outdoor) {
                if (Array.isArray(phase.outdoorTint)) {
                    finalTint = phase.outdoorTint.clone();
                }
            } else if (phase && $dataMap.meta.indoor) {
                if (Array.isArray(phase.indoorTint)) {
                    finalTint = phase.indoorTint.clone();
                }
            }  
        }

        for(var i = 0; i < 4; i++) finalTint[i] += this.data.currentWeatherTintMod;
        $gameScreen.startTint(finalTint, 10);

        if (cmz.debugMode)
            console.log("Final Tint: " + finalTint);
    }

    updateWeather() {
        var mon = this.struct.months[this.clock.date.month - 1];
        if (mon) {
            var ran = Math.random();
            if (ran < mon.rainChance) {
                this.data.currentWeather = "rain";
                this.data.currentWeatherSeverity = mon.severity + Math.random() - 0.5;
                this.data.currentWeatherTintMod = Math.round(-30 * this.data.currentWeatherSeverity);
            } else if (ran < mon.rainChance + mon.snowChance) {
                this.data.currentWeather = "snow";
                this.data.currentWeatherSeverity = mon.severity + Math.random() - 0.5;
                this.data.currentWeatherTintMod = Math.round(-30 * this.data.currentWeatherSeverity);
            } else {
                this.data.currentWeather = "none";
                this.data.currentWeatherSeverity = 0;
                this.data.currentWeatherTintMod = 0;
            }
        }
        this.renderWeather();
    }

    renderWeather() {
        if ($dataMap && ($dataMap.meta.indoor || !$dataMap.meta.outdoor)) {
            $gameScreen.changeWeather("none", 0, 0);
            return;
        }
        var currentWeather = this.data.currentWeather;
        var severity = this.data.currentWeatherSeverity;
        if (this.data.currentWeather == "rain") {
            if (severity > 0.7) {
                currentWeather = "storm";
                severity = (severity - 0.7) * 10 / 3;
            }
            else severity /= 0.7;
        }
        severity = Math.ceil(Math.min(1, severity) * 8);
        $gameScreen.changeWeather(currentWeather, severity + 1, 0);

        cmz.eval.Args.now.setWeather(currentWeather);
        mz.requestEventRefresh();

        if (cmz.debugMode)
            console.log("Weather: " + currentWeather + "@" + severity);
    }

    updateMapCondition() {
        this.updateWeather();
        this.updateTint();
    }

    initialize() {
        var thisVar = this;
        cmz.events.onGameLoad.after(function() {
            thisVar.clock.setData(thisVar.clock.fromTime(thisVar.data.time).getData());
            thisVar.clock.phase.maxActionPoint = thisVar.data.maxAP;
            cmz.eval.Args.now.setWeather(thisVar.data.currentWeather);
            cmz.eval.Args.now.setTime(cmz.plugins.datetime.clock.clone());
        });
        cmz.events.onFrameTick.after(function(delta) {
            thisVar.tick(delta / 60);
        });
        cmz.eval.Args.now = new CMZDateTimeInfo();
    }

    shouldTick() {
        return !mz.isEventRunning() && mz.isInMapScene() && mz.isGameActive();
    }

    tick(delta) {
        if (!this.struct.usePhase && this.useRTC && this.shouldTick()) {
            var old = this.clock.clone();
            this.clock.time.second += delta * this.speed;
            this.clock.correct();
            this.triggerTimeChanged(old);
        }
    }

    add(v) {
        var old = this.clock.clone();
        this.clock.add(v);
        this.triggerTimeChanged(old);
    }

    subtract(v) {
        var old = this.clock.clone();
        this.clock.subtract(v);
        this.triggerTimeChanged(old);
    }

    consumeActionPoint(v) {
        var old = this.clock.clone();
        this.clock.consumeActionPoint(v);
        this.triggerTimeChanged(old);
    }

    nextDay(jump) {
        jump = jump || 0;
        if (this.struct.usePhase) {
            var ph = jump % this.struct.phases.length;
            if (ph < 0) ph += this.struct.phases.length;
            var old = this.clock.clone();
            this.clock.phase.phase = ph;
            this.clock.phase.actionPoint = this.clock.phase.maxActionPoint;
        } else {
            var hour = jump % 24;
            if (hour < 0) hour += 24;
            var old = this.clock.clone();
            this.clock.time.hour = hour;
            this.clock.time.minute = 0;
            this.clock.time.second = 0;
        }
        this.clock.date.day++;
        this.clock.correct();
        this.triggerTimeChanged(old);
    }

    triggerTimeChanged(old) {
        var nw = this.clock;
        this.timeChanged(
            {
                second: old.time.second !== nw.time.second,
                minute: old.time.minute !== nw.time.minute,
                hour: old.time.hour !== nw.time.hour,
                
                day: old.date.day !== nw.date.day,
                month: old.date.month !== nw.date.month,
                year: old.date.year !== nw.date.day,
                
                phase: this.struct.usePhase ? old.phase.phase !== nw.phase.phase : this.struct.phase(old) !== this.struct.phase(nw),
            }
        );
    }

    timeChanged(changed) {
        this.data.time = this.clock.toTime();
        this.data.maxAP = this.clock.phase.maxActionPoint;
        if (cmz.eval.Args.now)
            cmz.eval.Args.now.setTime(this.clock.clone());

        if (changed.phase) {
            this.clock.phase.actionPoint = this.clock.phase.maxActionPoint;
            this.updateMapCondition();
        }
        
        mz.requestEventRefresh();
    }

    resetData() {
        this.clock = new CMZDateTimeValue(this.struct);
        this.timeChanged({});
    }

    constructor() {
        super();
        var prm = mz.pluginParameter("cmz.datetime");
        this.useRTC = prm.useRTC === true;
        this.speed = prm.realSecondToGameSecond || 60;

        this.struct = new CMZDateTimeStruct();
        this.struct.usePhase = prm.usePhase === true;
        this.struct.initialMaxAP = prm.initMaxAP || 1;      
        
        for(var i = 0; i < prm.months.length; i++) {
            var mn = prm.months[i];
            var m = new CMZDateTimeMonthStruct(mn.daysinmonth, mn.shortname, mn.longname);
            m.rainChance = mn.rainchance / 100;
            m.snowChance = mn.snowchance / 100;
            m.severity = mn.weatherseverity / 100;
            this.struct.months.push(m);
        }

        for(var i = 0; i < prm.daysinweek.length; i++) {
            var dw = prm.daysinweek[i];
            var d = new CMZDateTimeDayOfWeekStruct(dw.shortname, dw.longname);
            this.struct.daysOfWeek.push(d);
        }

        for(var i = 0; i < prm.phases.length; i++) {
            var ph = prm.phases[i];
            var d = new CMZDateTimePhaseStruct(ph.shortname, ph.longname);
            d.indoorTint = [ph.indoortint.red, ph.indoortint.green, ph.indoortint.blue, ph.indoortint.gray];
            d.outdoorTint = [ph.outdoortint.red, ph.outdoortint.green, ph.outdoortint.blue, ph.outdoortint.gray];
            d.start = ph.start;
            d.end = ph.end;
            this.struct.phases.push(d);
        }

        if (this.struct.usePhase) this.useRTC = false;
        this.resetData();
    }
}

class CMZDateTimeInfo {
    setWeather(weather) {
        this.weather = weather;
        this.isRainy = weather == "rain" || weather == "storm";
        this.isStormy = weather == "storm";
        this.isSnowy = weather == "snow";
        this.isClear = weather == "none";
    }

    setTime(value) {
        this.time = value;
        this.second = value.time.second;
        this.minute = value.time.minute;
        this.hour = value.time.hour;
        this.year = value.date.year;
        this.month = value.date.month;
        this.day = value.date.day;
        this.phase = value.phase.phase;
        this.actionPoint = value.phase.actionPoint;
        this.ap = value.phase.actionPoint;
        this.maxActionPoint = value.phase.maxActionPoint;
        this.maxap = value.phase.maxActionPoint;
        this.dayOfWeek = value.dayOfWeek();
        this.dow = value.dayOfWeek();

        for(var i = 0; i < value.struct.daysOfWeek.length; i++) {
            var dow = value.struct.daysOfWeek[i];
            this["is" + this._sanitizeName(dow.longName)] = i == this.dow;
        }

        for(var i = 0; i < value.struct.months.length; i++) {
            var mon = value.struct.months[i];
            this["is" + this._sanitizeName(mon.longName)] = i == (this.month - 1);
        }

        for(var i = 0; i < value.struct.phases.length; i++) {
            var phs = value.struct.phases[i];
            this["is" + this._sanitizeName(phs.longName)] = i == this.phase;
        }
    }

    yearBetween(a, b) {
        if (a > b) {
            return this.year <= a || this.year >= b;
        }
        return this.year >= a && this.year <= b;
    }

    monthBetween(a, b) {
        if (a > b) {
            return this.month <= a || this.month >= b;
        }
        return this.month >= a && this.month <= b;
    }

    dayBetween(a, b) {
        if (a > b) {
            return this.day <= a || this.day >= b;
        }
        return this.day >= a && this.day <= b;
    }

    phaseBetween(a, b) {
        if (a > b) {
            return this.phase <= a || this.phase >= b;
        }
        return this.phase >= a && this.phase <= b;
    }

    dowBetween(a, b) {
        if (a > b) {
            return this.dayOfWeek <= a || this.dayOfWeek >= b;
        }
        return this.dayOfWeek >= a && this.dayOfWeek <= b;
    }

    _sanitizeName(name) {
        return name.replace(/[^a-zA-Z]/g,"");
    }
}

class CMZDateTimeValue {
    __class = "CMZDateTimeValue";

    constructor(struct) {
        this.struct = struct;
        if (struct) {
            this.phase.actionPoint = this.phase.maxActionPoint = struct.initialMaxAP;
        }
    }

    time = {
        second: 0,
        minute: 0,
        hour: 0,
    }

    phase = {
        actionPoint: 0,
        maxActionPoint: 0,
        phase: 0,
    }

    date = {
        day: 1,
        month: 1,
        year: 1,
    }

    getData() {
        return {
            time: {
                second: this.time.second,
                minute: this.time.minute,
                hour: this.time.hour,
            },
            phase: {
                phase: this.phase.phase,
                actionPoint: this.phase.actionPoint,
                maxActionPoint: this.phase.maxActionPoint,
            },
            date: {
                day: this.date.day,
                month: this.date.month,
                year: this.date.year
            }
        }
    }

    setData(value) {
        this.time.second = value.time.second;
        this.time.minute = value.time.minute;
        this.time.hour = value.time.hour;
        this.phase.phase = value.phase.phase;
        this.phase.actionPoint = value.phase.actionPoint;
        this.phase.maxActionPoint = value.phase.maxActionPoint;
        this.date.day = value.date.day;
        this.date.month = value.date.month;
        this.date.year = value.date.year;
        this.correct();
    }

    add(v) {
        if (typeof v === "string") v = this.parse(v, false);
        this.time.second += v.time.second;
        this.time.minute += v.time.minute;
        this.time.hour += v.time.hour;
        this.phase.phase += v.phase.phase;
        this.date.day += v.date.day;
        this.correct();
    }

    subtract(v) {
        if (typeof v === "string") v = this.parse(v, false);
        this.time.second -= v.time.second;
        this.time.minute -= v.time.minute;
        this.time.hour -= v.time.hour;
        this.phase.phase -= v.phase.phase;
        this.date.day -= v.date.day;
        this.correct();
    }

    consumeActionPoint(ap) {
        if (ap === undefined) ap = 1;
        this.phase.actionPoint -= ap;
        this.correct();
    }

    correct() {
        if (this.struct.usePhase) {
            if (this.phase.actionPoint <= 0) {
                this.phase.actionPoint = this.phase.maxActionPoint;
                this.phase.phase++;
            }
            while(this.phase.phase < 0) {
                this.phase.phase += this.struct.phases.length;
                this.phase.actionPoint = this.phase.maxActionPoint;
                this.date.day--;
            }
            while(this.phase.phase >= this.struct.phases.length) {
                this.phase.phase -= this.struct.phases.length;
                this.phase.actionPoint = this.phase.maxActionPoint;
                this.date.day++;
            }
        } else {
            while(this.time.second < 0) {
                this.time.second += 60;
                this.time.minute--;
            }
            while(this.time.second >= 60) {
                this.time.second -= 60;
                this.time.minute++;
            }
            while(this.time.minute < 0) {
                this.time.minute += 60;
                this.time.hour--;
            }
            while(this.time.minute >= 60) {
                this.time.minute -= 60;
                this.time.hour++;
            }
            while(this.time.hour < 0) {
                this.time.hour += 24;
                this.date.day--;
            }
            while(this.time.hour >= 24) {
                this.time.hour -= 24;
                this.date.day++;
            }
        }

        // fix month first
        while(this.date.month < 1) {
            this.date.month += this.struct.months.length;
            this.date.year--;
        }
        while(this.date.month > this.struct.months.length) {
            this.date.month -= this.struct.months.length;
            this.date.year++;
        }

        while(this.date.day < 1) {
            this.date.day += this.struct.daysInMonth(this.date.month - 1);
            this.date.month--;
        }
        while(this.date.day > this.struct.daysInMonth(this.date.month)) {
            this.date.day -= this.struct.daysInMonth(this.date.month);
            this.date.month++;
        }

        // fix month again
        while(this.date.month < 1) {
            this.date.month += this.struct.months.length;
            this.date.year--;
        }
        while(this.date.month > this.struct.months.length) {
            this.date.month -= this.struct.months.length;
            this.date.year++;
        }
    }

    clone() {
        var res = new CMZDateTimeValue();
        res.time.second = this.time.second;
        res.time.minute = this.time.minute;
        res.time.hour = this.time.hour;
        res.phase.phase = this.phase.phase;
        res.phase.actionPoint = this.phase.actionPoint;
        res.phase.maxActionPoint = this.phase.maxActionPoint;
        res.date.day = this.date.day;
        res.date.month = this.date.month;
        res.date.year = this.date.year;
        res.struct = this.struct;
        return res;
    }
    
    parse(str, correct) {
        if (typeof str !== "string") return null;
        var res = new CMZDateTimeValue(this.struct);
        res.date.year = 0;
        res.date.month = 0;
        res.date.day = 0;
        var m;
        str = str.trim();
        if (!str) return null;

        const regex = /^(?:([0-9]{1,4})-([0-9]?[0-9])-([0-9]?[0-9]))?(?:T([0-2]?[0-9])(?::([0-5]?[0-9])(?::([0-5]?[0-9]))?)?|P([0-9]+)(?::([0-9]+))?)?$/;
        if ((m = regex.exec(str)) !== null) {            
            // The result can be accessed through the `m`-variable.
            m.forEach((match, groupIndex) => {  
                if (groupIndex == 1 && match) res.date.year = Number(match);
                if (groupIndex == 2 && match) res.date.month = Number(match);
                if (groupIndex == 3 && match) res.date.day = Number(match);
                if (groupIndex == 4 && match) res.time.hour = Number(match);
                if (groupIndex == 5 && match) res.time.minute = Number(match);
                if (groupIndex == 6 && match) res.time.second = Number(match);
                if (groupIndex == 7 && match) {
                    res.phase.phase = Number(match);
                    res.phase.actionPoint = 0;
                }
                if (groupIndex == 8 && match) res.phase.actionPoint = Number(match);
            });
        }

        if (correct === undefined || correct === true)
            res.correct();
        return res;
    }

    toString(format) {
        format = format || (this.struct.usePhase ? "yyyy-MM-ddPpp:aa" : "yyyy-MM-ddTHH:mm:ss");
        if (typeof format !== "string") return null;
        var formatter = {
            yyyy: String(this.date.year),
            yyy: String(this.date.year).padStart(3, "0"),
            yy: String(this.date.year).padStart(2, "0"),

            MMMM: this.struct.monthLongName(this.date.month),
            MMM: this.struct.monthShortName(this.date.month),
            MM: String(this.date.month).padStart(2, "0"),
            M: String(this.date.month),

            dddd: this.struct.weekdayLongName(this),
            ddd: this.struct.weekdayShortName(this),
            dd: String(this.date.day).padStart(2, "0"),
            d: String(this.date.day),

            HH: String(this.time.hour).padStart(2, "0"),
            H: String(this.time.hour),

            hh: String(this.time.hour % 12).padStart(2, "0"),
            h: String(this.time.hour % 12),

            mm: String(this.time.minute).padStart(2, "0"),
            m: String(this.time.minute),

            ss: String(Math.round(this.time.second)).padStart(2, "0"),
            s: String(Math.round(this.time.second)),

            N: this.time.hour < 12 ? "AM" : "PM",
            n: this.time.hour < 12 ? "am" : "pm",

            pppp: this.struct.phaseLongName(this.struct.phase(this)),
            ppp: this.struct.phaseShortName(this.struct.phase(this)),
            pp: String(this.struct.phase(this)).padStart(2, "0"),
            p: String(this.struct.phase(this)),

            aa: String(this.phase.actionPoint).padStart(2, "0"),
            a: String(this.phase.actionPoint),

            AA: String(this.phase.maxActionPoint).padStart(2, "0"),
            A: String(this.phase.maxActionPoint),
        };
        formatter.yyy = formatter.yyy.substring(formatter.yyy.length - 3);
        formatter.yy = formatter.yy.substring(formatter.yy.length - 2);

        var res = format;
        var ctr = 0;
        var rep = {};
        for(var k in formatter) {
            var idx = res.indexOf(k);
            while(idx >= 0) {
                res = res.replace(k, "{" + ctr + "}");
                rep["{" + ctr + "}"] = formatter[k];
                ctr++;
                idx = res.indexOf(k);
            }
        }
        for(var k in rep) {
            res = res.replace(k, rep[k]);
        }
        return res;
    }

    toTime(int) {
        return this.struct.toTime(this, int);
    }

    fromTime(val) {
        return this.struct.fromTime(val);
    }

    daysInMonth(mon) {
        return this.struct.daysInMonth(mon);
    }

    daysInYear() {
        return this.struct.daysInYear();
    }

    dayOfWeek() {
        return this.struct.dayOfWeek(this);
    }

    parseNormalize(str) {
        var st = this.parse(str, false);
        if (st.date.year == 0) { 
            st.date.year = this.date.year;
            if (st.date.month == 0) {
                st.date.month = this.date.month;
                if (st.date.day == 0) 
                    st.date.day = this.date.day;
            }
        }
        return st;
    }

    between(s, e) {
        var st = this.parseNormalize(s).toTime();
        var ed = this.parseNormalize(e).toTime();
        var tm = this.toTime();

        if (st <= ed) {
            return st <= tm && ed >= tm;
        } else {
            return st <= tm || ed >= tm;
        }
    }

    before(s) {
        var st = this.parseNormalize(s).toTime();
        var tm = this.toTime();

        return st >= tm;
    }

    after(s) {
        var st = this.parseNormalize(s).toTime();
        var tm = this.toTime();

        return st <= tm;
    }

    dowBetween(s, e) {
        var dow = this.dayOfWeek();
        if (s <= e)
            return s <= dow && e >= dow;
        return s <= dow || e >= dow;
    }
}

class CMZDateTimeStruct {
    __class = "CMZDateTimeStruct";

    months = [];
    daysOfWeek = [];
    usePhase = false;
    initialMaxAP = 0;
    phases = [];

    daysInYear() {
        var sum = 0;
        for(var i = 0; i < this.months.length; i++) {
            sum += this.months[i].daysInMonth;
        }
        return sum;
    }

    daysInMonth(mon) {
        var ix = (mon - 1) % this.months.length;
        if (ix < 0) ix += this.months.length;

        var m = this.months[ix];
        if (m) return m.daysInMonth;
        return null;
    }

    phase(cdatetime) {
        if (this.usePhase) {
            var p = cdatetime.phase.phase % this.phases.length;
            if (p < 0) p += this.phases.length;
            return p;
        } else {
            var hm = cdatetime.time.hour * 100 + cdatetime.time.minute;
            var match = 0;
            for(var i = 0; i < this.phases.length; i++) {
                var p = this.phases[i];
                var hms = p.start.hour * 100 + p.start.minute;
                var hme = p.end.hour * 100 + p.end.minute;
                if (hme >= hms) {
                    if (hm >= hms && hm <= hme) {
                        match = i;
                    }
                } else {
                    if (hm >= hms || hm <= hme) {
                        match = i;
                    }
                }
            }
            return match;
        }
    }

    phaseLongName(phase) {
        var ix = phase % this.phases.length;
        if (ix < 0) ix += this.phases.length;

        var m = this.phases[ix];
        if (m) return m.longName;
        return null;
    }

    phaseShortName(phase) {
        var ix = phase % this.phases.length;
        if (ix < 0) ix += this.phases.length;

        var m = this.phases[ix];
        if (m) return m.shortName;
        return null;
    }

    weekdayLongName(cdatetime) {
        var day = this.dayOfWeek(cdatetime);
        var ix = day % this.daysOfWeek.length;
        if (ix < 0) ix += this.daysOfWeek.length;

        var m = this.daysOfWeek[ix];
        if (m) return m.longName;
        return null;
    }

    weekdayShortName(cdatetime) {
        var day = this.dayOfWeek(cdatetime);
        var ix = day % this.daysOfWeek.length;
        if (ix < 0) ix += this.daysOfWeek.length;

        var m = this.daysOfWeek[ix];
        if (m) return m.shortName;
        return null;
    }

    monthLongName(mon) {
        var ix = (mon - 1) % this.months.length;
        if (ix < 0) ix += this.months.length;

        var m = this.months[ix];
        if (m) return m.longName;
        return null;
    }

    monthShortName(mon) {
        var ix = (mon - 1) % this.months.length;
        if (ix < 0) ix += this.months.length;

        var m = this.months[ix];
        if (m) return m.shortName;
        return null;
    }

    toTime(cdatetime, int) {
        var sum = cdatetime.date.day + cdatetime.date.year * this.daysInYear();
        for(var i = cdatetime.date.month - 1; i > 0; i--) {
            sum += this.daysInMonth(i);
        }
        sum--;
        if (!int) {
            if (!this.usePhase)
                sum += (cdatetime.time.hour * 3600 + cdatetime.time.minute * 60 + cdatetime.time.second) / (3600 * 24);
            else {
                if (typeof cdatetime.phase === "number")
                    sum += cdatetime.phase / 100;
                else
                    sum += cdatetime.phase.phase / 100 + cdatetime.phase.actionPoint / 10000;
            }
        }
        return sum;
    }

    fromTime(val) {
        val += 1;
        var frac = val % 1;
        var days = Math.floor(val);
        var y = Math.floor(days / this.daysInYear());
        var m = 1;
        days = days % this.daysInYear();
        for(var i = 1; i <= this.months.length; i++) {
            if (days > this.daysInMonth(i)) {
                days -= this.daysInMonth(i);
                m++;
            } else if (days <= this.daysInMonth(i)) {
                break;
            }
        }
        var d = days;

        var tm = Math.round(frac * 3600 * 24);
        var h = Math.floor(tm / 3600);
        var i = Math.floor((tm % 3600) / 60);
        var s = tm % 60;

        var pm = frac * 10000;
        var p = Math.round(pm / 100);
        var a = Math.round(pm % 100);

        var dt = new CMZDateTimeValue(this);
        return this.usePhase ? dt.parse(y + "-" + m + "-" + d + "P" + p + ":" + a) : dt.parse(y + "-" + m + "-" + d + "T" + h + ":" + i + ":" + s);
    }

    dayOfWeek(cdatetime) {
        var sum = this.toTime(cdatetime, true);
        return sum % this.daysOfWeek.length;
    }
}

class CMZDateTimeMonthStruct {
    __class = "CMZDateTimeMonthStruct";

    constructor(days, shortName, longName) {
        this.daysInMonth = Number(days);
        this.shortName = shortName;
        this.longName = longName;
    }

    rainChance = 0;
    snowChance = 0;
    severity = 0;
}

class CMZDateTimeDayOfWeekStruct {
    __class = "CMZDateTimeDayOfWeekStruct";

    constructor(shortName, longName) {
        this.shortName = shortName;
        this.longName = longName;
    }
}

class CMZDateTimePhaseStruct {
    __class = "CMZDateTimePhaseStruct";

    constructor(shortName, longName) {
        this.shortName = shortName;
        this.longName = longName;
    }
    indoorTint = [0,0,0,0];
    outdoorTint = [0,0,0,0];
    start = {hour: 0, minute: 0}
    end = {hour: 0, minute: 0}
}

cmz.events.onTimeChanged = new CMZFunctionHook(CMZDateTime, 'timeChanged');
cmz.events.onMapStart.after(function() {
    cmz.plugins.datetime.renderWeather();
    cmz.plugins.datetime.updateTint();
});
cmz.registerPlugin(new CMZDateTime());

// Register to plugin manager
PluginManager.registerCommand("cmz.datetime", "setTime", args => {
    var arg = mz.sanitizePluginParameter(args);
    var old = cmz.plugins.datetime.clock.clone();
    cmz.plugins.datetime.clock.setData(cmz.plugins.datetime.clock.parse(arg.value).getData());
    cmz.plugins.datetime.triggerTimeChanged(old);
});
PluginManager.registerCommand("cmz.datetime", "addTime", args => {
    var arg = mz.sanitizePluginParameter(args);
    cmz.plugins.datetime.add(arg.value);
});
PluginManager.registerCommand("cmz.datetime", "subtractTime", args => {
    var arg = mz.sanitizePluginParameter(args);
    cmz.plugins.datetime.subtract(arg.value);
});
PluginManager.registerCommand("cmz.datetime", "consumeAP", args => {
    var arg = mz.sanitizePluginParameter(args);
    cmz.plugins.datetime.consumeActionPoint(arg.value);
});
PluginManager.registerCommand("cmz.datetime", "nextPhase", args => {
    var arg = mz.sanitizePluginParameter(args);
    cmz.plugins.datetime.add("P1");    
});
PluginManager.registerCommand("cmz.datetime", "nextDay", args => {
    var arg = mz.sanitizePluginParameter(args);
    var toPhase = arg.phase || null;
    if (Number(toPhase) || toPhase === 0) {
        while(true) {
            cmz.plugins.datetime.add("P1");
            if (cmz.plugins.datetime.clock.phase.phase == toPhase) break;
        }
    }
});

// self register
cmz.events.onTimeChanged.after(function(c) {
    if (c) {
        if (c.day) {
            if (cmz.debugMode) console.log("Day has changed.")
            var prm = mz.pluginParameter("cmz.datetime");

            var vars = prm.dayChangeVariables;
            if (Array.isArray(vars)) {
                for(var i = 0; i < vars.length; i++) {
                    var cv = vars[i];
                    if (cv.variable > 0 && (!cv.cond || cmz.eval.exec(cv.cond))) {
                        $gameVariables.setValue(cv.variable, cv.value);
                    }
                }
            }

            vars = prm.dayChangeSwitches;
            if (Array.isArray(vars)) {
                for(var i = 0; i < vars.length; i++) {
                    var cv = vars[i];
                    if (cv.switch > 0 && (!cv.cond || cmz.eval.exec(cv.cond))) {
                        $gameSwitches.setValue(cv.switch, cv.value === true);
                    }
                }
            }
        }
        if (c.phase || c.day) {
            if (cmz.debugMode) console.log("Phase has changed.")
            var prm = mz.pluginParameter("cmz.datetime");

            var vars = prm.phaseChangeVariables;
            if (Array.isArray(vars)) {
                for(var i = 0; i < vars.length; i++) {
                    var cv = vars[i];
                    if (cv.variable > 0 && (!cv.cond || cmz.eval.exec(cv.cond))) {
                        $gameVariables.setValue(cv.variable, cv.value);
                    }
                }
            }

            vars = prm.phaseChangeSwitches;
            if (Array.isArray(vars)) {
                for(var i = 0; i < vars.length; i++) {
                    var cv = vars[i];
                    if (cv.switch > 0 && (!cv.cond || cmz.eval.exec(cv.cond))) {
                        $gameSwitches.setValue(cv.switch, cv.value === true);
                    }
                }
            }
        }
    }
});