class CMZQuest extends CMZPluginBase {
    _meta = {
        name : "CMZ Quest",
        id : "quest",
        author : "Code Atelier",
        version : new Version(1, 0, 0),
        cmzMinVersion : new Version(1, 1, 0),
        require : [],
    }

    data = {};

    db = {};

    constructor() {
        super();
        // extract quest and step data
        var par = mz.pluginParameter("cmz.quest");
        this.db = par.quests;

        this.showquestlog = par.showquestlog === undefined ? true : par.showquestlog === true;
        this.questlogname = par.questlogname === undefined ? "Quest Log" : par.questlogname;
        this.data = this.buildData();
        this.updateArgs();
    }

    initialize() {
        if (this.showquestlog) {
            cmz.runOnce("quest.questlog", function() {
                cmz.events.onAddMenuCustomCommand.after(function() {
                    this.caller.addCommand(cmz.plugins.quest.questlogname, "quest.log", cmz.plugins.quest.showquestlog);
                });
            });
            cmz.events.onCreateCommandWindow.after(function() {
                this.caller._commandWindow.setHandler("quest.log", () => {
                    SceneManager.push(Scene_QuestLog);
                });
            });
        }
        cmz.events.onGameLoad.after(function(data) {
            cmz.plugins.quest.data = cmz.plugins.quest.buildData(data.pluginData.quest);
            cmz.plugins.quest.updateArgs();
        });
        
        cmz.events.onConvertEscapeCharacter.after(function(text) {
            text = text.replace(/\\/g, '\x1b');
            text = text.replace(/\x1bq\[([0-9]+)\]/gi, function (match, id) {
                var item = cmz.plugins.quest.findDB(id);
                if (item)
                    return item.name;
                return match;
            });
            return text;
        });
    }

    buildData(savedData) {
        var res = [];
        savedData = savedData || [];
        for(var i = 0; i < this.db.length; i++) {
            var quest = this.db[i];
            var sdQuest = savedData[i];
            var qData = {
                id: quest.id,
                completed: sdQuest ? sdQuest.completed : false,
                started: sdQuest ? sdQuest.started : false,
                steps: [],
            };
            for(var j = 0; j < quest.steps.length; j++) {
                var step = quest.steps[j];
                var sdStep = sdQuest && sdQuest.steps ? sdQuest.steps[j] : null;
                var qStep = {
                    id: step.id,
                    objectives: [],
                    active: sdStep ? sdStep.active : false,
                    completed: sdStep ? sdStep.completed : false,
                };
                for(var k = 0; k < step.objectives.length; k++) {
                    var obj = step.objectives[k];
                    var sdObj = sdStep && sdStep.objectives ? sdStep.objectives[k] : null;
                    var qObj = {
                        id: obj.id,
                        counter: sdObj ? sdObj.counter : 0,
                        completed: sdObj ? sdObj.completed : false,
                    };
                    qStep.objectives.push(qObj);
                }
                qData.steps.push(qStep);
            }
            res.push(qData);
        }
        return res;
    }

    updateArgs() {
        var o = {};
        var completedQuests = [];
        for(var i = 0; i < this.data.length; i++) {
            var q = this.data[i];
            if (q.completed) completedQuests.push(q.id);
            var qo = {
                completed: q.completed,
                started: q.started,
            };

            for(var j = 0; j < q.steps.length; j++) {
                var s = q.steps[j];
                var so = {
                    completed: s.completed,
                    active: s.active,
                };

                for(var k = 0; k < s.objectives.length; k++) {
                    var t = s.objectives[k];
                    var to = {
                        completed: t.completed,
                        counter: t.counter
                    };
                    so["o" + t.id] = to;
                }

                qo["s" + s.id] = so;
            }

            o["q" + q.id] = qo;
        }
        o._completed = completedQuests;
        o.completed = function() {
            for(var i = 0; i < arguments.length; i++) {
                if (!this._completed.includes(arguments[i])) return false;
            }
            return true;
        }
        cmz.eval.Args.quest = o;
        mz.requestEventRefresh();
    }

    findDB(questId, stepId, objectiveId) {
        var res = null;
        for(var i = 0; questId && i < this.db.length; i++) {
            var quest = this.db[i];
            if (quest.id == questId) {
                res = quest;
                for(var j = 0; stepId && j < quest.steps.length; j++) {
                    var step = quest.steps[j];
                    if (step.id == stepId) {
                        res = step;
                        for(var k = 0; k < step.objectives.length; k++) {
                            var obj = step.objectives[k];
                            if (obj.id == objectiveId) {
                                res = obj;
                                break;
                            }
                        }
                        break;
                    }
                }
                break;
            }
        }
        return res;
    }

    find(questId, stepId, objectiveId) {
        var res = null;
        for(var i = 0; questId && i < this.data.length; i++) {
            var quest = this.data[i];
            if (quest.id == questId) {
                res = quest;
                for(var j = 0; stepId && j < quest.steps.length; j++) {
                    var step = quest.steps[j];
                    if (step.id == stepId) {
                        res = step;
                        for(var k = 0; k < step.objectives.length; k++) {
                            var obj = step.objectives[k];
                            if (obj.id == objectiveId) {
                                res = obj;
                                break;
                            }
                        }
                        break;
                    }
                }
                break;
            }
        }
        return res;
    }

    clearObjective(objData) {
        objData.completed = false;
        objData.counter = 0;
        this.updateArgs();
    }

    clearStep(stepData) {
        stepData.active = false;
        stepData.completed = false;
        for(var j = 0; j < stepData.objectives.length; j++) {
            var objData = stepData.objectives[j];
            this.clearObjective(objData);
        }
        this.updateArgs();
    }

    startQuest(id) {
        var questData = this.find(id);
        var questDb = this.findDB(id);
        if (questData) {
            if (!questData.started) {
                if (!questData.completed || questDb.repeatable) {
                    questData.started = true;
                    for(var i = 0; i < questData.steps.length; i++) {
                        var stepData = questData.steps[i];
                        this.clearStep(stepData);
                        stepData.active = i == 0;
                    }
                    this.updateArgs();
                }
            }
        }
    }

    completeStep(questData, stepData) {
        if (!questData || !stepData || !questData.started) return;
        stepData.active = false;
        stepData.completed = true;
        var newStepActivated = false;
        for(var i = 0; i < questData.steps.length; i++) {
            var step = questData.steps[i];
            if (step.completed) continue;
            else {
                this.clearStep(step);
                step.active = true;
                newStepActivated = true;
                break;
            }
        }
        if (!newStepActivated) {
            questData.started = false;
            questData.completed = true;
        }
        this.updateArgs();
    }

    _completeStep(qid, sid) {
        var questData = this.find(qid);
        var stepData = this.find(qid, sid);
        this.completeStep(questData, stepData);
    }

    progressObjective(questData, stepData, objData, increment) {
        if (!questData || !stepData || !objData || !questData.started || !stepData.active) return;
        var obj = this.findDB(questData.id, stepData.id, objData.id);
        if (obj) {
            if (obj.objectiveType) {
                objData.counter += increment;
                if (objData.counter >= obj.count) {
                    objData.completed = true;
                }
            } else {
                objData.completed = true;
            }
        }
        if (!stepData.objectives.any(x => !x.completed)) {
            this.completeStep(questData, stepData);
        }
        this.updateArgs();
    }

    _progressObjective(qid, sid, oid, increment) {
        var questData = this.find(qid);
        var stepData = this.find(qid, sid);
        var objData = this.find(qid, sid, oid);
        this.progressObjective(questData, stepData, objData, increment);
    }

    finish(id) {
    }

    cancel(id) {
    }
}


// Register to plugin manager
PluginManager.registerCommand("cmz.quest", "start", args => {
    var arg = mz.sanitizePluginParameter(args);
    cmz.plugins.quest.startQuest(arg.id);
});
PluginManager.registerCommand("cmz.quest", "completeStep", args => {
    var arg = mz.sanitizePluginParameter(args);
    cmz.plugins.quest._completeStep(arg.questid, arg.stepid);
});
PluginManager.registerCommand("cmz.quest", "progressObjective", args => {
    var arg = mz.sanitizePluginParameter(args);
    cmz.plugins.quest._progressObjective(arg.questid, arg.stepid, arg.objid, arg.increment);
});
PluginManager.registerCommand("cmz.quest", "cancel", args => {
    var arg = mz.sanitizePluginParameter(args);
    cmz.plugins.quest.cancel(arg.id);
});
PluginManager.registerCommand("cmz.quest", "finish", args => {
    var arg = mz.sanitizePluginParameter(args);
    cmz.plugins.quest.finish(arg.id);
});

// register to cmz
cmz.registerPlugin(new CMZQuest());

// === QUEST LOG ===
function Window_QuestLog() {
    this.initialize(...arguments);
}
Window_QuestLog.prototype = Object.create(Window_Selectable.prototype);
Window_QuestLog.prototype.constructor = Window_QuestLog;

Window_QuestLog.prototype.initialize = function(rect, descWindow) {
    Window_Selectable.prototype.initialize.call(this, rect);
    this._questLogDescWindow = descWindow;
    this.setHandler("ok", () => {});
    this.refresh();
    this.select(0);
    this.activate();
};
Window_QuestLog.prototype.refresh = function() {
    this.makeQuestList();
    Window_Selectable.prototype.refresh.call(this);
};
Window_QuestLog.prototype._list = [];
Window_QuestLog.prototype.expandMain = true;
Window_QuestLog.prototype.expandMainC = false;
Window_QuestLog.prototype.expandSub = true;
Window_QuestLog.prototype.expandSubC = false;
Window_QuestLog.prototype.maxItems = function() {
    return this._list.length;
};
Window_QuestLog.prototype.clearQuestList = function() {
    this._list = [];
};
Window_QuestLog.prototype.makeQuestList = function() {
    var qpl = cmz.plugins.quest;
    var db = cmz.plugins.quest.db;
    var data = cmz.plugins.quest.data;
    
    var main_progress = db.where(x => x.questType == true && !x.hidden && qpl.find(x.id).started);
    var main_completed = db.where(x => x.questType == true && !x.hidden && qpl.find(x.id).completed && !qpl.find(x.id).started);
    var sub_progress = db.where(x => x.questType == false && !x.hidden && qpl.find(x.id).started);
    var sub_completed = db.where(x => x.questType == false && !x.hidden && qpl.find(x.id).completed && !qpl.find(x.id).started);

    var all_displayable = [main_progress, sub_progress, main_completed, sub_completed];

    var qList = [];
    for(var i = 0; i < all_displayable.length; i++) {
        var li = all_displayable[i];
        qList.push({
            questType: i % 2 == 0 ? "main" : "side",
            isControl: true,
            active: i < 2,
        });
        if (i == 0 && !this.expandMain) continue;
        if (i == 1 && !this.expandSub) continue;
        if (i == 2 && !this.expandMainC) continue;
        if (i == 3 && !this.expandSubC) continue;
        for(var j = 0; j < li.length; j++) {
            var q = li[j];
            var qdata = qpl.find(q.id);
            var step = qdata.steps.where(x => x.active && !x.completed).first();
            qList.push({
                id: q.id,
                title: q.name,
                description: q.desc.split(/[\r\n↵]/),
                active: qdata.started,
                completed: qdata.completed,
                currentStep: step,
                rewards: q.rewards,
                questType: i % 2 == 0? "main" : "side",
                isControl: false
            });
        }
    }

    this._list = qList;
};
Window_QuestLog.prototype.itemAt = function(index) {
    return index >= 0 ? this._list[index] : null;
};
Window_QuestLog.prototype.drawItem = function(index) {
    var cur = this.itemAt(index);
    if (!cur) return;
    const rect = this.itemLineRect(index);
    const align = "left";
    this.resetTextColor();
    if (cur.isControl) {
        this.changePaintOpacity(true);
        if (cur.questType == "main")
            this.changeTextColor(ColorManager.textColor(cur.active ? 1 : 8));
        else 
            this.changeTextColor(ColorManager.textColor(cur.active ? 3 : 8));
        this.drawText( 
            cur.questType == "main" ? 
            ((this.expandMain && cur.active) || (this.expandMainC && !cur.active) ? "-" : "+") + " Main Quests - " + (cur.active ? "Active" : "Inactive")
            : ((this.expandSub && cur.active) || (this.expandSubC && !cur.active) ? "-" : "+") + " Side Quests - " + (cur.active ? "Active" : "Inactive")
            , rect.x, rect.y, rect.width, align);
    }
    else 
    {
        this.changePaintOpacity(cur.active);
        if (cur.questType == "main") {
            this.drawIcon(cur.active ? 165 : 161, rect.x + 20, rect.y + 2);
        } else {
            this.drawIcon(cur.active ? 4 : 20, rect.x + 20, rect.y + 2);
        }
        this.drawText(cur.title, rect.x + 58, rect.y, rect.width - 58, align);
    }
};
Window_QuestLog.prototype.select = function(index) {
    Window_Selectable.prototype.select.call(this, index);
    if (this._questLogDescWindow) {
        this._questLogDescWindow._selected = this.itemAt(index);
        this._questLogDescWindow.resetScroll();
        this._questLogDescWindow.refresh();
    }
};

Window_QuestLog.prototype.processOk = function() {
    var current = this.itemAt(this._index);
    if (current && current.isControl) {
        this.playOkSound();
        if (current.questType == "main" && current.active)
            this.expandMain = !this.expandMain;
        if (current.questType == "main" && !current.active)
            this.expandMainC = !this.expandMainC;
        if (current.questType == "side" && current.active)
            this.expandSub = !this.expandSub;
        if (current.questType == "side" && !current.active)
            this.expandSubC = !this.expandSubC;
        this.refresh();
    }
}

// === QUEST LOG TITLE ===
function Window_QuestLogTitle() {
    this.initialize(...arguments);
}

Window_QuestLogTitle.prototype = Object.create(Window_Selectable.prototype);
Window_QuestLogTitle.prototype.constructor = Window_QuestLogTitle;

Window_QuestLogTitle.prototype.initialize = function(rect) {
    Window_Selectable.prototype.initialize.call(this, rect);
    this.refresh();
};

Window_QuestLogTitle.prototype.colSpacing = function() {
    return 0;
};

Window_QuestLogTitle.prototype.refresh = function() {
    this.contents.clear();
    var rect = this.itemLineRect(0);
    this.drawText(cmz.plugins.quest.questlogname, 0, 0, rect.width, "center");
};

Window_QuestLogTitle.prototype.currencyUnit = function() {
    return TextManager.currencyUnit;
};

Window_QuestLogTitle.prototype.open = function() {
    this.refresh();
    Window_Selectable.prototype.open.call(this);
};

// === QUEST LOG DESC ===
function Window_QuestLogDescription() {
    this.initialize(...arguments);
}

Window_QuestLogDescription.prototype = Object.create(Window_Selectable.prototype);
Window_QuestLogDescription.prototype.constructor = Window_QuestLogDescription;
Window_QuestLogDescription.prototype._selected = null;
Window_QuestLogDescription.prototype._scroll = 0;

Window_QuestLogDescription.prototype.initialize = function(rect) {
    Window_Selectable.prototype.initialize.call(this, rect);
    this.refresh();
};

Window_QuestLogDescription.prototype.resetScroll = function() {
    this._scroll = 0;
};

Window_QuestLogDescription.prototype.refresh = function() {
    this.contents.clear();
    if (this._selected) {
        var q = this._selected;

        if (q.isControl) {
            return;
        }

        var rect = this.itemLineRect(0);
        var y = 0;

        // Draw Quest Title
        if (q.completed) {
            this.resetTextColor();
            this.changePaintOpacity(false);
            this.drawText(q.title, 0, rect.height * y++, rect.width, "left");
        } else {
            this.changeTextColor(ColorManager.textColor(1));
            this.changePaintOpacity(true);
            this.drawText(q.title, 0, rect.height * y++, rect.width, "left");
        }
        this.changePaintOpacity(true);

        // Draw description
        var desc = q.description;
        var maxRows = q.currentStep ? 2 : 8;
        this.resetTextColor();
        for(var i = 0; i < maxRows; i++) {
            this.drawText((desc[i + this._scroll] || ""), 0, rect.height * y++, rect.width, "left");
        }

        if (q.currentStep) {
            y++;
            var step = cmz.plugins.quest.findDB(q.id, q.currentStep.id);
            this.changeTextColor(ColorManager.textColor(6));
            this.drawText(step.name, 0, rect.height * y++, rect.width, "left");

            var activeObjective = step.objectives.where(x => cmz.plugins.quest.find(q.id, q.currentStep.id, x.id).completed == false);
            var clearedObjective = step.objectives.where(x => cmz.plugins.quest.find(q.id, q.currentStep.id, x.id).completed);
            var combined = [activeObjective, clearedObjective];
            var cnt = 0;
            var maxObj = 4;
            combined.each((e) => {
                e.each((o) => {
                    if (cnt >= maxObj) return;
                    cnt++;
                    var obj = cmz.plugins.quest.find(q.id, q.currentStep.id, o.id);
                    this.resetTextColor();
                    if (!obj.completed) {
                        this.changePaintOpacity(true);
                    } else {
                        this.changePaintOpacity(false);
                    }

                    var suffix = "";
                    if (o.objectiveType) {
                        suffix = " (" + obj.counter + "/" + o.count + ")";
                    }

                    this.drawText("* " + o.name + suffix, 0, rect.height * y++, rect.width, "left");
                    this.changePaintOpacity(true);
                });
            });
            if (cnt < maxObj) y+= maxObj - cnt;
        }

        y++;
        this.changeTextColor(ColorManager.textColor(3));
        this.drawText("Rewards", 0, rect.height * y++, rect.width, "left");
        this.resetTextColor();
        for(var i = 0; i < 4; i++) {
            var r = q.rewards[i];
            if (r) {
                var title = r.name;
                if (r.quantity)
                    title += " x" + r.quantity;
                var x = 0;
                if (r.icon >= 0) {
                    this.drawIcon(r.icon, x, rect.height * y);
                    x = 40;
                }
                this.drawText(title, x, rect.height * y++, rect.width - x, "left");
            }
        }
    }
};


// === QUEST LOG SCENE ===
function Scene_QuestLog() {
    this.initialize(...arguments);
}

Scene_QuestLog.prototype = Object.create(Scene_Base.prototype);
Scene_QuestLog.prototype.constructor = Scene_QuestLog;

Scene_QuestLog.prototype.initialize = function() {
    Scene_Base.prototype.initialize.call(this);
};

Scene_QuestLog.prototype.create = function() {
    Scene_Base.prototype.create.call(this);
    this.createBackground();
    this.createWindowLayer();
    this.createQuestLogTitleWindow();
    this.createQuestLogDescriptionWindow();
    this.createQuestLogWindow();
};

Scene_QuestLog.prototype.createBackground = function() {
    this._backgroundFilter = new PIXI.filters.BlurFilter();
    this._backgroundSprite = new Sprite();
    this._backgroundSprite.bitmap = SceneManager.backgroundBitmap();
    this._backgroundSprite.filters = [this._backgroundFilter];
    this.addChild(this._backgroundSprite);
    this.setBackgroundOpacity(192);
};

Scene_QuestLog.prototype.setBackgroundOpacity = function(opacity) {
    this._backgroundSprite.opacity = opacity;
};

Scene_QuestLog.prototype.createQuestLogWindow = function() {
    const rect = this.questlogWindowRect();
    const window = new Window_QuestLog(rect, this._questLogDescWindow);
    this.addWindow(window);
    window.setHandler("cancel", this.popScene.bind(this));
    this._questLogWindow = window;
};

Scene_QuestLog.prototype.questlogWindowRect = function() {
    const ww = Graphics.boxWidth / 2 - 5;
    const wh = Graphics.boxHeight - this.questlogheaderWindowRect().height - 10;
    const wx = 0;
    const wy = this.questlogheaderWindowRect().height + 10;
    return new Rectangle(wx, wy, ww, wh);
};

Scene_QuestLog.prototype.createQuestLogTitleWindow = function() {
    const rect = this.questlogheaderWindowRect();
    const window = new Window_QuestLogTitle(rect);
    this.addWindow(window);
    this._questLogTitleWindow = window;
};

Scene_QuestLog.prototype.questlogheaderWindowRect = function() {
    const ww = Graphics.boxWidth;
    const wh = this.calcWindowHeight(1, true);
    const wx = 0;
    const wy = 0;
    return new Rectangle(wx, wy, ww, wh);
};

Scene_QuestLog.prototype.createQuestLogDescriptionWindow = function() {
    const rect = this.questlogdescWindowRect();
    const window = new Window_QuestLogDescription(rect);
    this.addWindow(window);
    this._questLogDescWindow = window;
};

Scene_QuestLog.prototype.questlogdescWindowRect = function() {
    const ww = Graphics.boxWidth - this.questlogWindowRect().width - 10;
    const wh = Graphics.boxHeight - this.questlogheaderWindowRect().height - 10;
    const wx = this.questlogWindowRect().width + 10;
    const wy = this.questlogheaderWindowRect().height + 10;
    return new Rectangle(wx, wy, ww, wh);
};