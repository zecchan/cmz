//=============================================================================
// Coatl RPG Maker MZ - Quest Plugin
//=============================================================================

/*:
 * @target MZ
 * @plugindesc Provide quest system for RPG Maker MZ.
 * @author Zecchan Silverlake
 *
 * @help cmz.quest.js v1.0.0
 *
 * This plugin contains necessary logic for CMZ Plugins.
 * 
 * @param quests
 * @text Quest Database
 * @desc A list of available quests
 * @type struct<QuestStruct>[]
 * @default []
 * 
 * @param questlogshow
 * @text Show Quest Log
 * @desc Show the quest log in menu
 * @type boolean
 * @on Yes
 * @off No
 * @default true
 * 
 * @param questlogname
 * @text Quest Log Name
 * @desc The name of the quest log
 * @type string
 * @default Quest Log
 *
 * @command start
 * @text Start Quest
 * @desc Starts a quest and show it on quest log if not hidden. If quest already started, this does nothing.
 * 
 * @arg id
 * @text Quest ID
 * @desc The id of the quest
 * @type number
 *
 * @command completeStep
 * @text Complete Step
 * @desc Complete a step and activate the next step irrespective of objective. Quest and step must be active.
 * 
 * @arg questid
 * @text Quest ID
 * @desc The id of the quest
 * @type number
 * 
 * @arg stepid
 * @text Step ID
 * @desc The id of the quest step
 * @type number
 *
 * @command progressObjective
 * @text Progress Objective
 * @desc Increment objective counter or otherwise clear it. Also completes the step if all progress is done.
 * 
 * @arg questid
 * @text Quest ID
 * @desc The id of the quest
 * @type number
 * 
 * @arg stepid
 * @text Step ID
 * @desc The id of the quest step
 * @type number
 * 
 * @arg objid
 * @text Objective ID
 * @desc The id of the step objective
 * @type number
 * 
 * @arg increment
 * @text Increment
 * @desc The counter increment for the objective. When value is equal or greater than limit, the objective is cleared.
 * @type number
 * @default 0
 * 
 * @command cancel
 * @text Cancel Quest
 * @desc Cancels a quest and reset step progression. Quest must be started first.
 * 
 * @arg id
 * @text Quest ID
 * @desc The id of the quest
 * @type string
 * 
 * @command finish
 * @text Finish Quest
 * @desc Finishes a quest and update the quest log if not hidden. Quest must be started first.
 * 
 * @arg id
 * @text Quest ID
 * @desc The id of the quest
 * @type string
 */


/*~struct~QuestStruct:
 *
 * @param id
 * @text Quest ID
 * @type number
 * @desc The id of the quest. Must be unique.
 * @default
 * 
 * @param name
 * @text Quest Name
 * @type string
 * @desc The name of the quest.
 * @default
 * 
 * @param desc
 * @text Quest Description
 * @type multiline_string
 * @desc The description of the quest.
 * @default
 * 
 * @param hidden
 * @text Hidden Quest?
 * @desc When set to Yes, this quest will not show in quest log.
 * @type boolean
 * @on Yes
 * @off No
 * @default false
 * 
 * @param questType
 * @text Main Quest?
 * @desc Type of the quest.
 * @type boolean
 * @on Main Quest
 * @off Side Quest
 * @default true
 * 
 * @param repeatable
 * @text Repeatable?
 * @desc When set to Yes, this quest can be restarted.
 * @type boolean
 * @on Yes
 * @off No
 * @default false
 * 
 * @param steps
 * @text Steps
 * @desc A list of steps in this quest in-order.
 * @type struct<QuestStepStruct>[]
 * @default []
 * 
 * @param rewards
 * @text Rewards
 * @desc A list of rewards of this quest.
 * @type struct<QuestRewardStruct>[]
 * @default []
 * 
 */

/*~struct~QuestStepStruct:
 *
 * @param id
 * @text Step ID
 * @type number
 * @desc The id of the step. Must be unique for each quest.
 * @default
 * 
 * @param name
 * @text Step Name
 * @type string
 * @desc The name of the step.
 * @default
 * 
 * @param objectives
 * @text Objectives
 * @desc A list of objectives of this step.
 * @type struct<QuestStepObjectiveStruct>[]
 * @default []
 * 
 */

/*~struct~QuestStepObjectiveStruct:
 *
 * @param id
 * @text Objective ID
 * @type number
 * @desc The id of the objective. Must be unique for each step.
 * @default
 * 
 * @param name
 * @text Objective
 * @type string
 * @desc The objective of the step.
 * @default
 * 
 * @param objectiveType
 * @text Objective Type
 * @desc Type of the objective.
 * @type boolean
 * @on Counter
 * @off None
 * @default false
 * 
 * @param count
 * @text Counter
 * @type number
 * @desc When using counter type, this is the max number.
 * @default
 */

/*~struct~QuestRewardStruct:
 *
 * @param name
 * @text Reward Name
 * @type string
 * @desc The name of the reward.
 * @default
 * 
 * @param icon
 * @text Reward Icon
 * @type number
 * @desc The icon id of the reward. -1 means no icon.
 * @default -1
 * 
 * @param quantity
 * @text Reward Quantity
 * @type number
 * @desc The icon id of the reward. 0 means not shown.
 * @default 0
 * 
 */