//=============================================================================
// Coatl RPG Maker MZ - Chara Plugin
//=============================================================================

/*:
 * @target MZ
 * @plugindesc Contains functionality to manage in-game characters.
 * @author Zecchan Silverlake
 *
 * @help cmz.chara.js v1.0.0
 *
 * This plugin contains functionality to manage in-game characters.
 * 
 * This plugin contains commands that support placeholders:
 * {o} - Current character outfit
 * {id} - The character id
 * 
 * This plugin also extends database tags:
 * Actors:
 * 
 * Weapon:
 * <onlyfor:{actor_id}> - Restricts equipment to a certain actor (can have multiple, separate by space)
 * 
 * Armor:
 * <outfit:{name}> - Set the outfit of the character when this armor is equipped
 * <onlyfor:{actor_id}> - Restricts equipment to a certain actor (can have multiple, separate by space)
 * 
 * @param chardb
 * @text Character Database
 * @type struct<CharaStruct>[]
 * @default []
 * 
 * @param showHeart
 * @text Show Affection
 * @type boolean
 * @on Show
 * @off Hide
 * @default false
 * @desc Show a heart with color to show affection level on every character dialogue named with @<charid>
 * 
 * @param affectionHeart
 * @text Affection Heart
 * @type struct<HeartStruct>[]
 * @default ["{\"min\":\"0\",\"color\":\"black\"}","{\"min\":\"20\",\"color\":\"blue\"}","{\"min\":\"40\",\"color\":\"green\"}","{\"min\":\"60\",\"color\":\"yellow\"}","{\"min\":\"75\",\"color\":\"orange\"}","{\"min\":\"90\",\"color\":\"red\"}"]
 *
 * @param dimCutin
 * @text Dim Cutin
 * @type boolean
 * @desc Dim character cutin when not speaking using @ tag in name column
 * @on Yes
 * @off No
 * @default true
 * 
 * @command cutin
 * @text Manage Cutin
 * @desc Manage a character cutin.
 *
 * @arg charid
 * @type string
 * @text Character ID
 * @desc The character ID
 *
 * @arg visible
 * @type boolean
 * @text Visibility
 * @on Visible
 * @off Invisible
 * @desc Show or hide the cutin for this character.
 * @default true
 *
 * @arg image
 * @type multiline_string
 * @text Image Name
 * @desc The image name to show. You can use placeholders here. Use comma or newline to add multiple.
 * @default
 *
 * @arg animated
 * @type boolean
 * @text Animated
 * @on Animated
 * @off Not-Animated
 * @desc Animate the cutin (must have multiple images)
 * @default false
 *
 * @arg animefps
 * @type number
 * @text Animation FPS
 * @desc How many frames per second should the animation rendered
 * @default 5
 *
 * @arg classes
 * @type string
 * @text JSS Classes
 * @desc JSS classes to apply, separated by space
 * @default cutin
 *
 * @arg style
 * @type multiline_string
 * @text Display Style 
 * @desc JSS script
 *
 * @arg index
 * @type number
 * @text Picture Index
 * @default 0
 * @desc Picture index of the stand. Set to 0 for auto.
 * 
 * @command clearCutin
 * @text Clear Cutin
 * @desc Clear all character cutins.
 * 
 * @command showEvent
 * @text Show Event
 * @desc Show character event.
 *
 * @arg charid
 * @type string
 * @text Character ID
 * @desc The character ID
 *
 * @arg image
 * @type multiline_string
 * @text Image Name
 * @desc The image name to show. You can use placeholders here. Use comma or newline to add multiple.
 * @default
 *
 * @arg bgoverlay
 * @type string
 * @text Background Overlay
 * @desc The name of background overlay. Must be put in img/picture (not subfolder). Always fill the screen.
 * @default
 *
 * @arg classes
 * @type string
 * @text JSS Classes
 * @desc JSS classes to apply, separated by space
 * @default event
 *
 * @arg style
 * @type multiline_string
 * @text Display Style 
 * @desc JSS script
 * 
 * @command hideEvent
 * @text Hide Event
 * @desc Hide all character events.
 * 
 * @command affection
 * @text Change Affection
 * @desc Changes affection of a character.
 *
 * @arg charid
 * @type string
 * @text Character ID
 * @desc The character ID
 * 
 * @arg amount
 * @type number
 * @text Affection Modifier
 * @default 1
 * @desc The number to increase/decrease from affection. (use negative number to decrease)
 *
 * @arg lock
 * @type string
 * @text Lock ID
 * @desc Affection modifiers with the same lock id will not be applied more than once in a day. Blank = always available.
 * 
 * @command stats
 * @text Change Custom Stat
 * @desc Changes custom stat of a character.
 *
 * @arg charid
 * @type string
 * @text Character ID
 * @desc The character ID
 *
 * @arg stat
 * @type string
 * @text Stat ID
 * @desc Custom stat ID.
 * 
 * @arg amount
 * @type number
 * @text Custom Modifier
 * @default 1
 * @desc The number to increase/decrease from the custom stat. (use negative number to decrease)
 *
 * @arg lock
 * @type string
 * @text Lock ID
 * @desc Custom modifiers with the same lock id will not be applied more than once in a day. Blank = always available.
 * 
 * @command revealName
 * @text Reveal Name
 * @desc Reveal the name of the character, if hidden.
 *
 * @arg charid
 * @type string
 * @text Character ID
 * @desc The character ID
 * 
 * @command affProgression
 * @text Allow Affection Progression
 * @desc Allow affection increase/decrease and show affection heart if enabled.
 *
 * @arg charid
 * @type string
 * @text Character ID
 * @desc The character ID
 *
 * @arg value
 * @type boolean
 * @on Enable
 * @off Disable
 * @text Value
 * @desc Enable/disable affection progression
 * @default true
 * 
 * @command setstage
 * @text Set Stage
 * @desc Set stage of a character.
 *
 * @arg charid
 * @type string
 * @text Character ID
 * @desc The character ID
 *
 * @arg name
 * @type string
 * @text Stage Name
 * @desc The name of the stage. Blank will be ignored.
 * 
 * @arg value
 * @type number
 * @text Value
 * @default 0
 * @desc The new value for the stage
 * 
 * @command setflag
 * @text Set Flag
 * @desc Set flag of a character.
 *
 * @arg charid
 * @type string
 * @text Character ID
 * @desc The character ID
 *
 * @arg name
 * @type string
 * @text Flag Name
 * @desc The name of the flag. Blank will be ignored.
 * 
 * @arg value
 * @type boolean
 * @text Value
 * @on True
 * @off False
 * @default true
 * @desc The new value for the flag
 * 
 */

/*~struct~HeartStruct:
 *
 * @param min
 * @text Minimum Affection
 * @type number
 * @desc The minimum affection to show this color
 * @default 0
 * 
 * @param color
 * @text Color
 * @type string
 * @desc The color of the heart
 * @default black
 */

/*~struct~CharaStruct:
 *
 * @param id
 * @text ID
 * @type string
 * @desc The ID of the character. Only alphanumeric and no space, it must start with alphabet.
 * @default
 * 
 * @param name
 * @text Name
 * @type string
 * @desc The Name of the character. This is what will be displayed as the character name. Use @charid in dialogues.
 * @default
 * 
 * @param hideName
 * @text Hide Name
 * @type boolean
 * @desc When set to hide, the character name will changed to Hidden Name.
 * @on Hide
 * @off Show
 * @default false
 * 
 * @param hiddenName
 * @text Hidden Name
 * @type string
 * @desc The Hidden Name of the character.
 * @default ???
 * 
 * @param affProgression
 * @text Affection Progression
 * @type boolean
 * @desc When set to disable, the affection cannot be increased/decreased and heart will not be shown.
 * @on Enable
 * @off Disable
 * @default true
 * 
 * @param color
 * @text Color
 * @type string
 * @desc Automatically color character name if shown using @name.
 * @default white
 * 
 * @param actor
 * @text Actor ID
 * @type number
 * @desc Bind this character to an actor. 0 = unbind (Affects outfit and images based on equipment)
 * @default 0
 * 
 * @param eventPath
 * @text Base Event Path
 * @type string
 * @desc Base path for event images.
 * @default {id}/evt
 * 
 * @param cutinPath
 * @text Base Cutin Path
 * @type string
 * @desc Base path for cutin images.
 * @default {id}/ctn
 * 
 * @param defaultOutfit
 * @text Default Outfit
 * @type string
 * @desc The default value for outfit. {o}
 * @default default
 * 
 * @param defaultCharacterSprite
 * @text Default Sprite
 * @type string
 * @desc The default sprite for the character.
 * @default
 * 
 * @param defaultCharacterSpriteIndex
 * @text Default Sprite Index
 * @type number
 * @desc The default sprite index for the character.
 * @default 0
 * 
 * @param defaultCutinStyle
 * @text Default Cutin Style
 * @type multiline_string
 * @desc Default style for cutins.
 * @default
 * 
 * @param cutinSets
 * @text Cutin Sets
 * @type struct<CharaSetStruct>[]
 * @default []
 * 
 */


/*~struct~CharaSetStruct:
 *
 * @param id
 * @text ID
 * @type string
 * @desc The ID of the set. Only alphanumeric and no space, it must start with alphabet.
 * @default
 * 
 * @param image
 * @type multiline_string
 * @text Image Name
 * @desc The image name to show. You can use placeholders here. Use comma or newline to add multiple.
 * @default
 *
 * @param animated
 * @type boolean
 * @text Animated
 * @on Animated
 * @off Not-Animated
 * @desc Animate the cutin (must have multiple images)
 * @default false
 *
 * @param animefps
 * @type number
 * @text Animation FPS
 * @desc How many frames per second should the animation rendered
 * @default 5
 *
 * @param classes
 * @type string
 * @text JSS Classes
 * @desc JSS classes to apply, separated by space
 * @default cutin
 *
 * @param style
 * @type multiline_string
 * @text Display Style 
 * @desc JSS script
 * 
 */