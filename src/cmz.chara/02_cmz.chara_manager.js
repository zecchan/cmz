

// Register to plugin manager
PluginManager.registerCommand("cmz.chara", "cutin", args => {
    var arg = mz.sanitizePluginParameter(args);
    cmz.plugins.chara.cutin(arg);
});
PluginManager.registerCommand("cmz.chara", "clearCutin", args => {
    var arg = mz.sanitizePluginParameter(args);
    cmz.plugins.chara.clearCutin(arg);
});
PluginManager.registerCommand("cmz.chara", "showEvent", args => {
    var arg = mz.sanitizePluginParameter(args);
    cmz.plugins.chara.showEvent(arg);
});
PluginManager.registerCommand("cmz.chara", "hideEvent", args => {
    var arg = mz.sanitizePluginParameter(args);
    cmz.plugins.chara.hideEvent(arg);
});
PluginManager.registerCommand("cmz.chara", "affProgression", args => {
    var arg = mz.sanitizePluginParameter(args);
    cmz.plugins.chara.affectionProgression(arg);
});
PluginManager.registerCommand("cmz.chara", "revealName", args => {
    var arg = mz.sanitizePluginParameter(args);
    cmz.plugins.chara.revealName(arg);
});
PluginManager.registerCommand("cmz.chara", "affection", args => {
    var arg = mz.sanitizePluginParameter(args);
    var chr = cmz.plugins.chara.db[arg.charid];
    if (chr && chr.affectionProgression()) {
        var lock = typeof arg.lock === "string" ? arg.lock.trim() : null;
        if (!lock || !cmz.plugins.chara.data.__affectionLocks[lock])
            cmz.plugins.chara.data.__affections[arg.charid] = (cmz.plugins.chara.data.__affections[arg.charid] || 0) + (Number(arg.amount) || 0);
        if (!!lock)
            cmz.plugins.chara.data.__affectionLocks[lock] = true;
        cmz.plugins.chara.updateEvalArgs();
        mz.requestEventRefresh();
    }
});
PluginManager.registerCommand("cmz.chara", "stats", args => {
    var arg = mz.sanitizePluginParameter(args);
    var chr = cmz.plugins.chara.db[arg.charid];
    if (chr) {
        var lock = typeof arg.lock === "string" ? arg.lock.trim() : null;
        var stat = typeof arg.stat === "string" ? arg.stat.trim() : null;
        if (!lock || !cmz.plugins.chara.data.__cstatsLocks[lock]) {
            cmz.plugins.chara.data.__cstats[arg.charid] = cmz.plugins.chara.data.__cstats[arg.charid] || {};
            if (stat) {
                var amt = Number(arg.amount) || 0;
                cmz.plugins.chara.data.__cstats[arg.charid][stat] = cmz.plugins.chara.data.__cstats[arg.charid][stat] || 0;
                cmz.plugins.chara.data.__cstats[arg.charid][stat] += amt;
            }
        }
        if (!!lock)
            cmz.plugins.chara.data.__cstatsLocks[lock] = true;
        cmz.plugins.chara.updateEvalArgs();
        mz.requestEventRefresh();
    }
});