class CMZChara extends CMZPluginBase {
    constructor() {
        super();

        var db = mz.pluginParameter("cmz.chara").chardb;
        this.dim = mz.pluginParameter("cmz.chara").dim !== false;
        this.db = {};
        this.frameCounter = 0;
        for(var i = 0; i < db.length; i++) {
            this.db[db[i].id] = new CMZCharaData(db[i]);
        }
    }

    dim = true;

    _meta = {
        name : "CMZ Chara",
        id : "chara",
        author : "Code Atelier",
        version : new Version(1, 0, 0),
        cmzMinVersion : new Version(1, 0, 0),
        require : [
        ],
    }

    data = {
        __shownPictures: {},
        __anime: {},
        
        __affections: {},
        __affectionLocks: {},

        __cstats: {},
        __cstatsLocks: {},

        __charStages: {},
        __charFlags: {},
    };

    initialize() {
        cmz.events.onConvertEscapeCharacter.before(function(text) {
            var char = null;
            text = text.replace(/\@(\w+)/g, function(match) {
                var id = match.substr(1);
                var chr = cmz.plugins.chara.db[id];
                if (chr) {
                    char = chr;
                    return chr.getName();
                }
                return match;
            });

            var that = this.caller;
            if (text.includes(":=") && that instanceof Window_NameBox) {
                var spl = text.split(":=", 2);
                if (spl.length == 2) {
                    text = spl[0];
                    if (char !== null) {
                        var arg = {
                            charid: char.id,
                            visible: true,
                            image: spl[1].split(";").join("\r\n"),
                            classes: "cutin",
                            style: ""
                        };
                        cmz.plugins.chara.cutin(arg);
                    }
                }
            }
            if (text.includes(":#") && that instanceof Window_NameBox) {
                var spl = text.split(":#", 2);
                if (spl.length == 2) {
                    text = spl[0];
                    if (char !== null) {
                        if (Array.isArray(char.cutinSets)) {
                            for(var i = 0; i < char.cutinSets.length; i++) {
                                var cs = char.cutinSets[i];
                                if (cs.id == spl[1].trim()) {
                                    var cln = Object.assign({}, cs);
                                    cln.charid = char.id;
                                    cln.visible = true;
                                    delete(cln.id);
                                    cmz.plugins.chara.cutin(cln);
                                }
                            }
                        }
                    }
                }
            }

            return text;
        });

        cmz.events.onGenerateMessageText.after(function() {
            var padEnd = "";
            var dimExcept = null;
            if ($gameMessage._speakerName) {
                var matches = $gameMessage._speakerName.match(/\@(\w+)/g);
                if (matches && matches.length > 0) {
                    var cid = matches[0].substr(1);
                    var chr = cmz.plugins.chara.db[cid];
                    dimExcept = chr.id;
                    if (chr && mz.pluginParameter("cmz.chara").showHeart && $gameMessage._texts) {
                        var hMap = mz.pluginParameter("cmz.chara").affectionHeart;
                        if (chr && chr.data().affProgression && Array.isArray(hMap)) {
                            var max = -9999;
                            var aff = cmz.plugins.chara.data.__affections[chr.id] || 0;                        
                            var col = null;
                            for(var i = 0; i < hMap.length; i++) {
                                if (max < hMap[i].min && aff >= hMap[i].min) {
                                    max = hMap[i].min;
                                    col = hMap[i].color;
                                }
                            }
                            if (col) {
                                padEnd = " \\c[" + col + "]♥\\c[0]";
                            } else {
                                padEnd = " ♥";
                            }
                        }
                    }
                }
            }

            if (cmz.plugins.chara.dim) {
                for(var k in cmz.plugins.chara.data.__shownPictures) {
                    var arr = cmz.plugins.chara.data.__shownPictures[k];
                    if (Array.isArray(arr)) {
                        for(var i = 0; i < arr.length; i++) {
                            var itm = arr[i];
                            if (k != dimExcept) {
                                $gameScreen.picture(itm)._tone = [-70,-70,-70,0];
                                $gameScreen.picture(itm)._opacity = 240;
                            } else {
                                $gameScreen.picture(itm)._tone = null;
                                $gameScreen.picture(itm)._opacity = 255;
                            }
                        }                        
                    }
                }
            }

            return this.caller._texts.join("\n") + padEnd;
        });

        cmz.events.onClearMessageText.after(function() {
        });

        cmz.events.onTick.after(function(delta) {
            if (mz.isGameActive()) {
                cmz.plugins.chara.frameCounter += delta;
                cmz.plugins.chara.animatorRefresh();
            }
        });

        cmz.events.onTimeChanged.after(function(changed) {
            if (changed.day) {
                cmz.plugins.chara.data.__affectionLocks = {};
                cmz.plugins.chara.data.__cstatsLocks = {};
                cmz.plugins.chara.updateEvalArgs();
            }
        });

        cmz.events.onGameLoad.after(function() {
            cmz.plugins.chara.data.__shownPictures = cmz.plugins.chara.data.__shownPictures || {};
            cmz.plugins.chara.data.__anime = cmz.plugins.chara.data.__anime || {};
            cmz.plugins.chara.data.__affections = cmz.plugins.chara.data.__affections || {};
            cmz.plugins.chara.data.__affectionLocks = cmz.plugins.chara.data.__affectionLocks || {};
            cmz.plugins.chara.data.__cstats = cmz.plugins.chara.data.__cstats || {};
            cmz.plugins.chara.data.__cstatsLocks = cmz.plugins.chara.data.__cstatsLocks || {};
            cmz.plugins.chara.updateEvalArgs();
        });
    }

    animatorRefresh() {
        for(var k in this.db) {
            this.cutinShowAnimated(k);
        }
    }

    getNewCutinIndex() {
        // start from 50
        var max = 49;
        for(var k in this.data.__shownPictures) {
            var a = this.data.__shownPictures[k];
            if (Array.isArray(a)) {
                for(var i = 0; i < a.length; i++) {
                    max = Math.max(max, a[i]);
                }
            }
        }
        return max + 1;
    }

    getNewEventIndex() {
        // start from 20
        var max = 19;
        for(var i = 0; i < this.data.__shownEvents.length; i++) {
            var a = this.data.__shownEvents[i];
            max = Math.max(max, a);
        }
        return max + 1;
    }

    cutinShowAnimated(chrId) {
        this.data.__anime = this.data.__anime || {};
        var anime = this.data.__anime[chrId];
        if (anime) {
            var div = 1000 / anime.fps;
            var deltaTime = Math.round(cmz.plugins.chara.frameCounter / div) % anime.frames.length || 0;
            var frame = anime.frames[deltaTime] || null;
            var chr = cmz.plugins.chara.db[chrId];
            if (frame) {
                var idx = this.data.__shownPictures[chrId][0];
                var path = chr.getCutin(frame);
                var sty = chr.defaultCutinStyle || "";
                if (sty && anime.style.trim()) sty += ",";
                sty += anime.style;
                cmz.images.showPicture(idx, path, anime.classes, sty);
            }
        }
    }

    cutin(arg) {
        this.data.__anime = this.data.__anime || {};
        var chr = this.db[arg.charid];
        if (chr) {
            var a = this.data.__shownPictures[chr.id];
            if (Array.isArray(a)) {
                cmz.images.clearPicture(a);
            }
            this.data.__shownPictures[chr.id] = [];
            this.data.__anime[chr.id] = null;

            if (arg.visible) {
                var spl = arg.image.split(/,|\r|\n/).select(x => x.trim()).where(x => !!x);
                var stidx = arg.index || this.getNewCutinIndex();
                if (!arg.animated) {
                    for(var i = 0; i < spl.length; i++) {
                        var path = chr.getCutin(spl[i]);
                        var idx = stidx;
                        stidx++;
                        var sty = chr.defaultCutinStyle || "";
                        if (sty && arg.style.trim()) sty += ",";
                        sty += arg.style;
                        cmz.images.showPicture(idx, path, arg.classes, sty);
                        this.data.__shownPictures[chr.id].push(idx);
                    }
                } else {
                    this.data.__shownPictures[chr.id] = [stidx];
                    this.data.__anime[chr.id] = {
                        fps: arg.animefps,
                        frames: spl,
                        style: arg.style,
                        classes: arg.classes
                    };
                    this.cutinShowAnimated(chr.id);
                }
            }
        }
    }

    clearCutin() {
        this.data.__anime = this.data.__anime || {};
        for(var k in this.db) {
            this.data.__anime[k] = null;
            var a = this.data.__shownPictures[k];
            if (Array.isArray(a)) {
                cmz.images.clearPicture(a);
                this.data.__shownPictures[k] = [];
            }
        }
    }

    showEvent(arg) {
        var chr = this.db[arg.charid];
        if (chr) {
            this.data.__shownEvents = this.data.__shownEvents || [];

            var spl = arg.image.split(/,|\r|\n/).select(x => x.trim()).where(x => !!x);
            var stidx = arg.index || this.getNewEventIndex();

            if (arg.bgoverlay) {
                var path = arg.bgoverlay;
                var idx = stidx;
                var sty = '"s":"fill"';
                cmz.images.showPicture(idx, path, "", sty);
                this.data.__shownEvents.push(idx);
                stidx++;
            }

            for(var i = 0; i < spl.length; i++) {
                var path = chr.getEvent(spl[i]);
                var idx = stidx;
                stidx++;
                var sty = chr.defaultEventStyle || "";
                if (sty && arg.style.trim()) sty += ",";
                sty += arg.style;
                cmz.images.showPicture(idx, path, arg.classes, sty);
                this.data.__shownEvents.push(idx);
            }
        }
    }

    hideEvent() {
        var a = this.data.__shownEvents;
        if (Array.isArray(a)) {
            cmz.images.clearPicture(a);
        }
        this.data.__shownEvents = [];
    }

    revealName(arg) {
        var chr = this.db[arg.charid];
        if (chr) {
            chr.reveal(true);
        }
    }

    affectionProgression(arg) {
        var chr = this.db[arg.charid];
        if (chr) {
            chr.affectionProgression(arg.value);
        }
    }

    updateEvalArgs() {
        var o = {};
        var locks = {};
        for(var r in this.data.__affectionLocks) {
            locks[r] = this.data.__affectionLocks[r];
        }
        for(var k in this.db) {
            var hMap = mz.pluginParameter("cmz.chara").affectionHeart || [];
            var aff = this.data.__affections[k] || 0;
            var afflv = 0;
            var affmx = -9999;
            for(var i = 0; i < hMap.length; i++) {
                if (hMap[i].min <= aff && aff > affmx) {
                    affmx = hMap[i].min;
                    afflv = i + 1;
                }
            }
            o[k] = {
                affection: aff,
                affectionLevel: afflv,
                __cstats: this.data.__cstats[k] || {},
                flags: this.data.__charFlags ? this.data.__charFlags[k] || {} : {},
                stages: this.data.__charStages ? this.data.__charStages[k] || {} : {},
            };
            o[k].cstat = function(id) {
                if (this.__cstats[id]) {
                    return this.__cstats[id] || 0;
                }
                return 0;
            }
        }
        cmz.eval.Args.char = o;
        cmz.eval.Args.charlocks = locks;
    }

    // getter
    getAffection(charid) {
        var chr = this.db[charid];
        if (chr) {
            return this.data.__affections[charid];
        }
        return 0;
    }
}

class CMZCharaData {
    constructor(db) {
        for(var k in db) this[k] = db[k];
    }

    flag(name, value) {
        if (value === undefined) {
            var flags = cmz.plugins.chara.data.__charFlags[this.id];
            if (!flags) return false;
            return flags[name] ? true : false;
        } else {
            if (!cmz.plugins.chara.data.__charFlags[this.id])
                cmz.plugins.chara.data.__charFlags[this.id] = {};
            cmz.plugins.chara.data.__charFlags[this.id][name] = value ? true : false;
            mz.requestEventRefresh();
        }
    }

    stage(name, value) {
        if (value === undefined) {
            var flags = cmz.plugins.chara.data.__charStages[this.id];
            if (!flags) return undefined;
            return flags[name];
        } else {
            if (!cmz.plugins.chara.data.__charStages[this.id])
                cmz.plugins.chara.data.__charStages[this.id] = {};
            cmz.plugins.chara.data.__charStages[this.id][name] = value;
            mz.requestEventRefresh();
        }
    }

    data(value) {
        if (value === undefined) {
            if (!cmz.plugins.chara.data[this.id])
                cmz.plugins.chara.data[this.id] = {};
            return cmz.plugins.chara.data[this.id];
        }
        else
            cmz.plugins.chara.data[this.id] = value;
    }
    
    getName() {
        if (this.hideName && this.data().revealed !== true)
            return "\\c[" + this.color + "]" + this.hiddenName + "\\c[0]";
        return "\\c[" + this.color + "]" + this.name + "\\c[0]";
    }

    getOutfit() {
        var o = this.data().forcedOutfit;
        if (!o) {

        }
        return o || "";
    }

    processPlaceholder(text) {
        text = text.replace("{id}", this.id);
        text = text.replace("{o}", this.getOutfit());
        return text;
    }

    getCutin(path) {
        if (path.substr(0,1) != "/")
            path = this.cutinPath + "/" + path;
        else
            path = path.substr(1);
        return this.processPlaceholder(path);
    }

    getEvent(path) {
        if (path.substr(0,1) != "/")
            path = this.eventPath + "/" + path;
        else
            path = path.substr(1);
        return this.processPlaceholder(path);
    }

    reveal(val) {
        this.data().revealed = val !== false;
    }

    affectionProgression(val) {
        if(val !== undefined) {
            this.data().affProgression = val !== false;
        }
        var val  =this.data().affProgression;
        if (val !== undefined)
            return val;
        return this.affProgression;
    }
}

cmz.registerPlugin(new CMZChara());